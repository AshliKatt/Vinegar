package dev.ashli.vinegar.cli

import dev.ashli.vinegar.lang.compile.target.CompileTarget
import dev.ashli.vinegar.encoding.encodeProjectData
import dev.ashli.vinegar.files.createFile
import dev.ashli.vinegar.printing.Message
import dev.ashli.vinegar.project.CURRENT_VERSION
import dev.ashli.vinegar.project.ProjectData
import java.io.File
import java.nio.file.Path
import kotlin.io.path.exists

private const val defaultProgramCode = """// Auto-generated example program
    
// Namespaces help organized code
namespace example {
    
    // Create the join event
    @EventHandler
    fn onJoin(event: JoinEvent) {
    
        // Send a message to the default player
        let player = event.defaultPlayer; 
        player.sendMessage(T"Hello, World!");
    }
    
}
"""

fun parseCreation(workingDir: Path, args: MutableList<String>) {
    // Get name
    val name = prompt("Choose a project name : ",
        null,
        "A folder with that name already exists in this directory, please choose a different name") {
        !workingDir.resolve(it).toFile().exists()
    }

    // Directory of project
    val projectDir = workingDir.resolve(name)

    // Preset
    println("  1. Basic")
    println("  2. Project (Default)")
    println("  3. Library")
    val preset = prompt("Select a project preset [1-3] : ", "2") {
        listOf("1", "2", "3").contains(it)
    }

    val settings = when (preset) {
        "1" -> {
            val target = buildSystemPrompt()
            ProjectData(name, defaultCompileTarget = target, version = CURRENT_VERSION)
        }
        "2" -> {
            val target = buildSystemPrompt()
            ProjectData(name, libFolder = "libs", defaultCompileTarget = target, version = CURRENT_VERSION)
        }
        "3" -> {
            ProjectData(name, defaultCompileTarget = CompileTarget.LIBRARY, version = CURRENT_VERSION)
        }
        else -> throw Error("Unreachable. Contact developers if this occurs.") // Impossible
    }

    if (projectDir.exists()) {
        Message.CREATE_PROJECT_FOLDER_EXISTS.println()
        return
    }

    // Attempt to make the project dir
    if (!projectDir.toFile().mkdir()) {
        Message.CREATE_PROJECT_FOLDER_FAIL.println()
        return
    }

    if (!createFile(projectDir.resolve("vinegar.json").toFile(), encodeProjectData(settings))) {
        return
    }

    // Make src, out, and lib folder
    if (tryMakeDir(projectDir.resolve(settings.sourceFolder).toFile())) {
        createFile(
            projectDir.resolve(settings.sourceFolder).resolve("main.vn").toFile(),
            defaultProgramCode
        )
    }

    tryMakeDir(projectDir.resolve(settings.outputFolder).toFile())

    if (settings.libFolder != null) tryMakeDir(projectDir.resolve(settings.libFolder).toFile())

    // Create .gitignore
    createFile(projectDir.resolve(".gitignore").toFile(), "${settings.outputFolder}/")

    Message.PROJECT_CREATED.println(settings.name)
}

private fun tryMakeDir(file: File): Boolean {
    if (!file.mkdir()) {
        Message.INIT_PROJECT_FAIL.println(file.nameWithoutExtension)
        return false
    }

    return true
}