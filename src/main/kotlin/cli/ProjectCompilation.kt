package dev.ashli.vinegar.cli

import dev.ashli.vinegar.lang.compile.state.ProgramCompletionState
import dev.ashli.vinegar.lang.error.VinegarError
import dev.ashli.vinegar.lang.error.warning.WarningManager
import dev.ashli.vinegar.files.FileManager
import dev.ashli.vinegar.printing.Message
import dev.ashli.vinegar.printing.Printing
import dev.ashli.vinegar.project.ProjectData
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.InvalidPathException
import java.nio.file.Path

fun parseCompile(workingDir: Path, args: MutableList<String>, performTests: Boolean) {
    // Get the actual path
    val actualPath = if (args.isNotEmpty()) {
        val arg = args.removeFirst()

        try {
            Path.of(arg)
        } catch (e: InvalidPathException) {
            Message.INVALID_PATH.println()
            return
        }
    } else {
        workingDir
    }

    val file = actualPath.toFile()

    // End if input file doesn't exist
    if (!file.exists()) {
        Message.NONEXISTENT_PATH.println()
        return
    }

    // Dir vs file
    if (file.isDirectory) {
        compileProjectFolder(file)
    } else if (file.isFile) {
        if (file.name == "vinegar.json") { // Project file
            val parent = file.parentFile

            if (parent == null) {
                Message.PROJECT_FILE_HAS_NO_PARENT.println()
                return
            }

            compileProjectFolder(parent)

        } else if (file.extension == "vinegar") {
            compileSingleFile(file)

        } else {
            Message.INVALID_FILE_FORMAT.println()

        }
    } else {
        Message.INVALID_FILE.println()
        return
    }
}

private fun compileProjectFolder(folder: File) {
    val settingsFile = folder.toPath().resolve("vinegar.json").toFile()

    if (!settingsFile.exists()) {
        Message.NO_PROJECT_FILE_COMPILE.println()
        return
    }

    val settings = settingsFile.bufferedReader().use {
        try {
            Json.decodeFromString<ProjectData>(it.readText())
        } catch (e: Error) {
            Message.MALFORMED_PROJECT_FILE.println()
            return
        }
    }

    val root = folder.toPath()

    val srcFolder = resolveProjectFolder(root, settings.sourceFolder) ?: return
    val outFolder = resolveProjectFolder(root, settings.outputFolder) ?: return

    if (settings.libFolder != null) {
        val libFolder = resolveProjectFolder(root, settings.libFolder) ?: return
    }

    FileManager.setOutput(outFolder)
    FileManager.trackFile(srcFolder)
    // TODO Track libs

    compile()
}

private fun resolveProjectFolder(root: Path, name: String): File? {
    try {
        val file = root.resolve(name).toFile()

        if (file.exists()) {
            return file
        } else {
            Message.NO_FOLDER_WITH_NAME.println(name)
            return null
        }
    } catch (e: InvalidPathException) {
        Message.MALFORMED_FOLDER_NAME.println(name)
        return null
    }
}

private fun compileSingleFile(file: File) {
    FileManager.trackFile(file)
    compile()
}

private fun compile() {
    try {
        Printing.printCompilingMessage()

        FileManager.compileAll()

        if (WarningManager.warningCount() == 0) {
            ProgramCompletionState.Success.printLine()
        } else {
            println()
            WarningManager.printWarnings()
            println()
            ProgramCompletionState.Warning.printLine()
        }

    } catch (e: VinegarError) {
        println()
        WarningManager.printWarnings()
        e.print()

        if (debugMode) {
            e.printStackTrace()
        }
        
        println()

        ProgramCompletionState.Error.printLine()
    }
}