package dev.ashli.vinegar.cli

import dev.ashli.vinegar.printing.Format
import dev.ashli.vinegar.printing.Message
import dev.ashli.vinegar.printing.Printing
import java.nio.file.Path

internal var debugMode = false

fun parseCommand(workingDir: Path, args: Array<String>) {
    // Parse flags
    val realArguments = mutableListOf<String>()
    val flags = mutableSetOf<String>()

    for (arg in args) {
        if (arg.startsWith("--")) {
            flags.add(arg)
        } else if (arg.startsWith('-')) {
            arg.substring(1).forEach { flags.add(it.lowercase()) }
        } else {
            realArguments.add(arg)
        }
    }

    // Apply flags
    if (flags.contains("f") || flags.contains("noformat")) Format.disableColors()
    if (flags.contains("d") || flags.contains("debug")) {
        debugMode = true
    }


    // Real commands
    if (realArguments.isEmpty()) {
        Message.NO_ARGUMENTS.println()
        return
    }

    when (realArguments.removeFirst()) {
        "help" -> Printing.printHelp()
        "new" -> parseCreation(workingDir, realArguments)
        "compile" -> parseCompile(workingDir, realArguments, false)
        "test" -> parseCompile(workingDir, realArguments, true)
    }
}