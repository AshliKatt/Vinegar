package dev.ashli.vinegar.cli

import dev.ashli.vinegar.lang.compile.target.CompileTarget
import dev.ashli.vinegar.printing.Format

// TODO Redo this entire thing
internal fun prompt(
    question: String,
    defaultResponse: String? = null,
    invalidResponse: String = "This is not a valid answer, please provide correct input",
    allowed: (String) -> Boolean
): String {
    while (true) {
        print("${Format.LOGO}Vinegar:${Format.RESET} $question")
        val response = readlnOrNull()
        println()

        if (response.isNullOrEmpty()) {
            if (defaultResponse != null) {
                return defaultResponse
            } else {
                println("${Format.WARNING}Vinegar:${Format.RESET} This question has no default answer. Please provide a valid input.")
            }
        } else {
            if (allowed(response)) {
                return response
            } else {
                println("${Format.WARNING}Vinegar:${Format.RESET} $invalidResponse.")
            }
        }
    }
}

internal fun booleanPrompt(
    question: String,
    defaultResponse: Boolean = true,
    invalidResponse: String = "This is not a valid answer, please provide correct input",
): Boolean {
    val out = prompt(question, if (defaultResponse) { "y" } else { "n" }) {
        listOf("y", "n", "yes", "no").contains(it.lowercase())
    }

    return out.lowercase().startsWith("y")
}


internal fun buildSystemPrompt() : CompileTarget {
    println("1. Commands (Default)")
    println("2. Recode")
    println("3. CodeClient")

    val option = prompt("Select default build system [1..3] : ", "1") {
        listOf("1", "2", "3").contains(it)
    }

    return when (option) {
        "1" -> CompileTarget.COMMANDS
        "2" -> CompileTarget.RECODE
        "3" -> CompileTarget.CODECLIENT
        else -> throw Error("unreachable")
    }
}