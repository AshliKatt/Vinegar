package dev.ashli.vinegar.printing

import dev.ashli.vinegar.files.FileManager
import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.printing.Format.Companion.CODE
import dev.ashli.vinegar.printing.Format.Companion.CODE_SEPARATOR
import dev.ashli.vinegar.printing.Format.Companion.LINE_NUMBER
import dev.ashli.vinegar.printing.Format.Companion.RESET
import kotlin.math.max
import kotlin.math.min

object Printing {
    private const val MAX_LINE_SIZE = 80
    private val separator = "=".repeat(MAX_LINE_SIZE)
    private var firstMessage = true

    fun messageOutline(h: () -> Unit) {

        if (firstMessage) {
            println(separator)
            firstMessage = false
        }
        println()
        h()
        println()
        println(separator)
    }

    fun newMessageBlock() {
        firstMessage = true
    }

    fun printCode(range: FileRangeLocation, highlight: Format, growBefore: Int, growAfter: Int): Int {
        // List of relevant lines for printing
        val lines = mutableListOf<String>()

        // Stream of the file's lines
        range.file.bufferedReader().use { reader ->
            val skippedLines = max(0, range.line - 1 - growBefore)
            val extraLinesToGrab = min(growBefore, skippedLines)

            // Skip the first few lines until we reach the first relevant line
            repeat (skippedLines) {
                reader.readLine()
            }

            // Add relevant lines to the list of lines to be printed
            repeat(max(0, range.lineEnd - range.line + growAfter + extraLinesToGrab + 1)) {
                val line = reader.readLine()
                if (line != null) {
                    lines.add(line)
                }
            }
        }

        // Length of the largest line number
        val lineNumberSpace = range.lineEnd.plus(growAfter).toString().length

        // Length of the longest line
        val maxLength = lines.maxOf(String::length)

        for ((index, line) in lines.withIndex()) {
            val lineNumber = range.line - growBefore + index

            // Print line prefix
            val lineNumberString = lineNumber.toString().padStart(lineNumberSpace, ' ')
            print("$LINE_NUMBER $lineNumberString $CODE_SEPARATOR| ")

            val col = range.col - 1
            val colEnd = range.colEnd - 1

            val printLine = line.padEnd(maxLength, ' ')

            // Print code
            if (range.line == range.lineEnd) {
                // Range is entirely on one line
                if (lineNumber == range.line) {
                    // We're on the relevant line
                    val before = printLine.substring(0, col)
                    val middle = printLine.substring(col, colEnd)
                    val after = printLine.substring(colEnd)
                    println("$CODE$before$highlight$middle$CODE$after $RESET")
                } else {
                    // We're not on the relevant line, print line as normal
                    println("$CODE$printLine $RESET")
                }

            } else {
                // Range spans multiple lines
                if (lineNumber < range.line || lineNumber > range.lineEnd) {
                    // Not on a relevant line, print as normal
                    println("$CODE$printLine $RESET")
                } else if (lineNumber == range.line) {
                    // First line of range
                    val before = printLine.substring(0, col)
                    val after = printLine.substring(col)
                    println("$CODE$before$highlight$after $RESET")
                } else if (lineNumber == range.lineEnd) {
                    // Last line of range
                    val before = printLine.substring(0, colEnd)
                    val after = printLine.substring(colEnd)
                    println("$highlight$before$CODE$after $RESET")
                } else {
                    // Middle line of range
                    println("$highlight$printLine $RESET")
                }

            }
        }

        return lineNumberSpace + 4
    }

    fun printCompilingMessage() {
        if (FileManager.fileCount() == 1) {
            println("${Format.LOGO}Vinegar:$RESET Compiling 1 file...")
        } else {
            println("${Format.LOGO}Vinegar:$RESET Compiling ${FileManager.fileCount()} files...")
        }
    }

    fun printHelp() {
        println("Flags:")
        println("  --noformat, -f : Prints output without any formatting.")
        println("  --debug,    -d : Prints JVM error information.")
        println()
        println("Subcommands:")
        println("  help :")
        println("    Displays this menu.")
        println("  new :")
        println("    Creates a new Vinegar project in the current working directory.")
        println("  compile [directory] :")
        println("    Compiles the Vinegar project at the specified location, or the current directory if omitted.")
    }
}