package dev.ashli.vinegar.printing

class Format(private val foreground: dev.ashli.vinegar.printing.Color?, private val background: dev.ashli.vinegar.printing.Color?, private val extra: String?) {
    companion object {
        private var allowColors = true

        fun disableColors() {
            dev.ashli.vinegar.printing.Format.Companion.allowColors = false
        }

        val RESET = dev.ashli.vinegar.printing.Format(null, null, "\u001B[0m")
        val BOLD = "\u001B[1m"

        val LINE_NUMBER =
            dev.ashli.vinegar.printing.Format(
                dev.ashli.vinegar.printing.Color.Companion.CODE,
                dev.ashli.vinegar.printing.Color.Companion.CODE_BACKGROUND,
                null
            )
        val CODE_SEPARATOR = dev.ashli.vinegar.printing.Format(
            dev.ashli.vinegar.printing.Color.Companion.CODE_LINE,
            dev.ashli.vinegar.printing.Color.Companion.CODE_BACKGROUND,
            null
        )
        val CODE =
            dev.ashli.vinegar.printing.Format(
                dev.ashli.vinegar.printing.Color.Companion.CODE,
                dev.ashli.vinegar.printing.Color.Companion.CODE_BACKGROUND,
                null
            )
        val CODE_ERROR = dev.ashli.vinegar.printing.Format(
            dev.ashli.vinegar.printing.Color.Companion.CODE_ERROR,
            dev.ashli.vinegar.printing.Color.Companion.CODE_BACKGROUND,
            null
        )
        val CODE_WARNING = dev.ashli.vinegar.printing.Format(
            dev.ashli.vinegar.printing.Color.Companion.CODE_WARNING,
            dev.ashli.vinegar.printing.Color.Companion.CODE_BACKGROUND,
            null
        )
        val HELP =
            dev.ashli.vinegar.printing.Format(dev.ashli.vinegar.printing.Color.Companion.HELP, null, null)
        val ERROR =
            dev.ashli.vinegar.printing.Format(dev.ashli.vinegar.printing.Color.Companion.CODE_ERROR, null, null)
        val WARNING = dev.ashli.vinegar.printing.Format(
            dev.ashli.vinegar.printing.Color.Companion.CODE_WARNING,
            null,
            null
        )

        val LOGO = dev.ashli.vinegar.printing.Format(
            dev.ashli.vinegar.printing.Color.Companion.LOGO_MAIN,
            null,
            dev.ashli.vinegar.printing.Format.Companion.BOLD
        )
        val LOGO_ERR =
            dev.ashli.vinegar.printing.Format(
                dev.ashli.vinegar.printing.Color.Companion.LOGO_ERR,
                null,
                dev.ashli.vinegar.printing.Format.Companion.BOLD
            )
        val LOGO_WARN =
            dev.ashli.vinegar.printing.Format(
                dev.ashli.vinegar.printing.Color.Companion.LOGO_WARN,
                null,
                dev.ashli.vinegar.printing.Format.Companion.BOLD
            )

        /**
         * Checks for strings in SCREAMING_SNAKE_CASE format
         */
        fun isScreamingSnake(s: String): Boolean {
            return s.uppercase() == s
        }

        /**
         * Checks for strings in UpperCamelCase format
         */
        fun isFormattedTypeName(s: String): Boolean {
            val firstChar = s[0]
            return firstChar == firstChar.uppercaseChar() && s.uppercase() != s
        }

        /**
         * Checks for strings in camelCase format.
         */
        fun isFormattedIdentifier(s: String): Boolean {
            val firstChar = s[0]
            return firstChar != firstChar.uppercaseChar() && s.uppercase() != s
        }
    }
    override fun toString(): String {
        if (!dev.ashli.vinegar.printing.Format.Companion.allowColors) return ""
        return "${getFGColor(foreground)}${getBGColor(background)}${extra ?: ""}"
    }

    private fun getFGColor(c: dev.ashli.vinegar.printing.Color?) = if (c == null) {
        ""
    } else {
        "\u001B[38;2;${c}m"
    }

    private fun getBGColor(c: dev.ashli.vinegar.printing.Color?) = if (c == null) {
        ""
    } else {
        "\u001B[48;2;${c}m"
    }
}