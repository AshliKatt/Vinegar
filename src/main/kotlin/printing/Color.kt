package dev.ashli.vinegar.printing

class Color(private val r: UByte, private val g: UByte, private val b: UByte) {
    companion object {
        val CODE_BACKGROUND = Color(50u, 50u, 50u)
        val CODE = Color(255u, 255u, 255u)
        val CODE_ERROR = Color(255u, 100u, 100u)
        val CODE_LINE = Color(150u, 150u, 150u)
        val HELP = Color(100u, 200u, 250u)
        val CODE_WARNING = Color(255u, 200u, 130u)

        val LOGO_MAIN = Color(0u, 144u, 96u)
        val LOGO_ERR = CODE_ERROR
        val LOGO_WARN = CODE_WARNING
    }
    override fun toString() = "$r;$g;$b"
}