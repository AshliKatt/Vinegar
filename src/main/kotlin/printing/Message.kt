package dev.ashli.vinegar.printing

private fun err(msg: String) = "${Format.LOGO_ERR}Vinegar:${Format.RESET} $msg"
private fun warn(msg: String) = "${Format.LOGO_WARN}Vinegar:${Format.RESET} $msg"
private fun success(msg: String) = "${Format.LOGO}Vinegar:${Format.RESET} $msg"

enum class Message(private val message: String) {
    NO_ARGUMENTS(err("No arguments given, use --help for help.")),
    NO_PROJECT_FILE(err("No project file (vinegar.json) found in working directory.")),

    CREATE_PROJECT_FOLDER_FAIL(err("Could not create project folder.")),
    CREATE_PROJECT_FOLDER_EXISTS(err("Folder with that name already exists in the current directory.")),
    CREATE_PROJECT_FILE_FAIL(err("Could not create file %s.")),
    INIT_PROJECT_FAIL(warn("Could not create non-essential project file or directory '%s'.")),

    PROJECT_CREATED(success("Created project %s!.")),
    NO_PROJECT_FILE_COMPILE(err("No project file (vinegar.json) found in input directory.")),

    INVALID_PATH(err("Input path is formatted incorrectly.")),
    INVALID_FILE(err("Input path does not point to a file or folder.")),
    INVALID_FILE_FORMAT(err("Input path does not point to a vinegar file or valid vinegar project.")),
    NONEXISTENT_PATH(err("Input path does not exist.")),
    PROJECT_FILE_HAS_NO_PARENT(err("Project file has no parent directory.")),
    MALFORMED_PROJECT_FILE(err("Malformed project file. (vinegar.json)")),
    MALFORMED_FOLDER_NAME(err("Invalid folder name for '%s'")),
    NO_FOLDER_WITH_NAME(err("Could not find project sub-directory '%s'.")),

    ;


    fun println() {
        kotlin.io.println(this.message)
    }

    fun print() {
        kotlin.io.print(this.message)
    }

    fun println(vararg args: Any) {
        kotlin.io.println(String.format(this.message, *args))
    }

    fun print(vararg args: Any) {
        kotlin.io.print(String.format(this.message, *args))
    }
}