package dev.ashli.vinegar.encoding

import dev.ashli.vinegar.project.ProjectData
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

private val PRETTY_JSON = Json {
    encodeDefaults = true
    prettyPrint = true
}

private val REPLACEMENT_STRATEGY = Regex("\\[\\s*]")

fun encodeProjectData(s: ProjectData): String {
    val out = PRETTY_JSON.encodeToString(s)
    return out.replace(REPLACEMENT_STRATEGY, "[]")
}
