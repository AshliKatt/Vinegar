package dev.ashli.vinegar

import dev.ashli.vinegar.cli.parseCommand
import dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget
import dev.ashli.vinegar.lang.transpiling.ast.Template
import dev.ashli.vinegar.lang.transpiling.ast.blocks.Bracket
import dev.ashli.vinegar.lang.transpiling.ast.blocks.PlayerAction
import dev.ashli.vinegar.lang.transpiling.ast.blocks.PlayerEvent
import dev.ashli.vinegar.lang.transpiling.ast.values.args.Argument
import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str
import dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import java.nio.file.Path
import java.nio.file.Paths

val WORKING_DIRECTORY: Path = Paths.get("").toAbsolutePath()

fun main(args: Array<String>) {
    val encoder = Json {
        encodeDefaults = true

        serializersModule = SerializersModule {
            polymorphic(Value::class, Str::class, Str.serializer())
        }
    }

    parseCommand(WORKING_DIRECTORY, args)
}