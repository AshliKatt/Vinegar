package dev.ashli.vinegar.number

import kotlin.math.absoluteValue


fun formatNum(x: Long): String {
    val mainNum = x / 1000
    val decimal = (x % 1000).absoluteValue

    if (decimal == 0L) {
        return "$mainNum"
    }

    return if (mainNum == 0L) {
        "0.${decimal.toString().padStart(3, '0')}"
    } else {
        "$mainNum.${decimal.toString().padStart(3, '0')}"
    }
}