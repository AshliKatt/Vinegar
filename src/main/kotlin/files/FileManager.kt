package dev.ashli.vinegar.files

import dev.ashli.vinegar.lang.compile.tracking.Tracker
import dev.ashli.vinegar.lang.parsing.core.parse
import dev.ashli.vinegar.lang.tokenization.Tokenizer
import dev.ashli.vinegar.lang.tokenization.settings.DefaultTokenizer
import java.io.File

/**
 * Keeps track of all files that are to be compiled
 */
object FileManager {
    private val files = mutableSetOf<File>()
    private var outputFile: File? = null

    /**
     * Tracks a file or directory as part of compilation.
     */
    fun trackFile(file: File) {
        file.walk().forEach {
            if (it.isFile && it.extension == "vn") {
                files.add(it)
            }
        }
    }

    fun fileCount() = files.size

    /**
     * Compile all tracked files with each-other as link targets
     */
    fun compileAll() {
        val resourceContext = Tracker.trackTrees(files.map {
            parse(Tokenizer.tokenize(it, DefaultTokenizer), it)
        })


        // TODO
    }

    fun setOutput(f: File) {
        outputFile = f
    }
}