package dev.ashli.vinegar.files

import dev.ashli.vinegar.printing.Message
import java.io.File
import java.io.IOException

fun createFile(file: File, text: String): Boolean {
    // Attempt to create vinegar.json
    if (!file.createNewFile()) {
        Message.CREATE_PROJECT_FILE_FAIL.println(file.name)
        return false
    }

    file.bufferedWriter().use {
        try {
            it.write(text)
        } catch (e: IOException) {
            Message.CREATE_PROJECT_FILE_FAIL.println(file.name)
            return false
        }
    }

    return true
}

