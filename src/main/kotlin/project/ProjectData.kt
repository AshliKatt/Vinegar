package dev.ashli.vinegar.project

import dev.ashli.vinegar.lang.compile.target.CompileTarget
import kotlinx.serialization.Serializable

val CURRENT_VERSION = "1.0.0"

/**
 * Holds relevant information to be stored in vinegar.json
 */
@Serializable
class ProjectData(
    val name: String,
    val description: String = "",
    val authors: List<String> = emptyList(),

    val sourceFolder: String = "src",
    val outputFolder: String = "out",
    val libFolder: String? = null,

    val externalLibs: List<String> = emptyList(),

    val defaultCompileTarget: CompileTarget = CompileTarget.COMMANDS,

    val version: String
)

