package dev.ashli.vinegar.helper

fun <T> T.applyIf(x: Boolean, wrapper: (T) -> T) = if (x) wrapper(this) else this