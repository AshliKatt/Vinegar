package dev.ashli.vinegar.lang.error

import dev.ashli.vinegar.lang.location.FileLocation
import dev.ashli.vinegar.printing.Format
import dev.ashli.vinegar.printing.Format.Companion.CODE
import dev.ashli.vinegar.printing.Format.Companion.ERROR
import dev.ashli.vinegar.printing.Format.Companion.RESET
import dev.ashli.vinegar.printing.Printing
import kotlin.math.max

open class VinegarError(
    message: String,
    val loc: FileLocation,
    val help: String?
) : Error("$message\n  At: line ${loc.line}, col ${loc.col}\n  In: ${loc.file.path}") {
    val line = loc.line
    val col = loc.col

    open fun print() {
        val actualCol = max(col - 1, 0)

        Printing.messageOutline {
            printError()
            val offset = Printing.printCode(loc.range(), CODE, 2, 0)
            println(" ".repeat(actualCol + offset) + "$ERROR^ Here $RESET")
            printHelp()
        }
    }

    internal fun printHelp() {
        if (help != null) {
            println()
            print(Format.HELP.toString() + "Help: ")
            println(help)
            print(RESET)
        }
    }

    internal fun printError() {
        println("${ERROR}Error: $message$RESET")
        println()
    }
}

