package dev.ashli.vinegar.lang.error

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.printing.Format.Companion.CODE_ERROR
import dev.ashli.vinegar.printing.Printing


open class RangeVinegarError(
    message: String,
    val rangeLoc: FileRangeLocation,
    help: String?
) : VinegarError(message, rangeLoc.point(), help) {
    val endLine = rangeLoc.lineEnd
    val endCol = rangeLoc.colEnd

    override fun print() {
        Printing.messageOutline {
            printError()
            Printing.printCode(rangeLoc, CODE_ERROR, 2, 2)
            printHelp()
        }
    }
}