package dev.ashli.vinegar.lang.error.warning

import dev.ashli.vinegar.lang.location.FileLocation
import dev.ashli.vinegar.printing.Format.Companion.CODE
import dev.ashli.vinegar.printing.Format.Companion.RESET
import dev.ashli.vinegar.printing.Format.Companion.WARNING
import dev.ashli.vinegar.printing.Printing
import kotlin.math.max

open class VinegarWarning(
    val message: String,
    val loc: FileLocation,
    val help: String?
) {
    val line = loc.line
    val col = loc.col

    open fun print() {
        val actualCol = max(col - 1, 0)

        Printing.messageOutline {
            printWarning()
            val offset = Printing.printCode(loc.range(), CODE, 2, 0)
            println(" ".repeat(actualCol + offset) + "$WARNING^ Here $RESET")
            printHelp()
        }
    }

    internal fun printHelp() {
        if (help != null) {
            println()
            print(dev.ashli.vinegar.printing.Format.HELP.toString() + "Help: ")
            println(help)
            print(RESET)
        }
    }

    internal fun printWarning() {
        println("${WARNING}Warning: $message\n" +
                "  At: line ${loc.line}, col ${loc.col}\n" +
                "  In: ${loc.file.path}$RESET")
        println()
    }

    private fun forceRaise() {
        WarningManager.generateWarning(this)
    }

    internal open fun tryRaise(): Boolean {
        return true
    }

    fun raise() {
        if (tryRaise()) {
            forceRaise()
        }
    }


}

