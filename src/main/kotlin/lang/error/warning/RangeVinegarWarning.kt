package dev.ashli.vinegar.lang.error.warning

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.parsing.tree.ConstDeclarationNode
import dev.ashli.vinegar.lang.parsing.tree.FunctionDefNode
import dev.ashli.vinegar.lang.parsing.tree.VarDeclarationNode
import dev.ashli.vinegar.printing.Format
import dev.ashli.vinegar.printing.Format.Companion.CODE_WARNING
import dev.ashli.vinegar.printing.Printing


open class RangeVinegarWarning(
    message: String,
    val rangeLoc: FileRangeLocation,
    help: String?
) : VinegarWarning(message, rangeLoc.point(), help) {
    val endLine = rangeLoc.lineEnd
    val endCol = rangeLoc.colEnd

    override fun print() {
        Printing.messageOutline {
            printWarning()
            Printing.printCode(rangeLoc, CODE_WARNING, 2, 2)
            printHelp()
        }
    }

    class InvalidConstFormat(
        val const: ConstDeclarationNode
    ) : RangeVinegarWarning(
        "Global constant names should be written with all capital letters.",
        const.location,
        "Change ${const.constName} to be all capital letters and underscores."
    ) {
        override fun tryRaise(): Boolean {
            return !Format.isScreamingSnake(const.constName)
        }
    }

    class InvalidVarFormat(
        val variable: VarDeclarationNode
    ) : RangeVinegarWarning(
        "Variable names should be written in camelCase.",
        variable.location,
        null
    ) {
        override fun tryRaise(): Boolean {
            return !Format.isFormattedIdentifier(variable.varName)
        }
    }

    class InvalidFunctionFormat(
        val func: FunctionDefNode
    ) : RangeVinegarWarning(
        "Function names should be written in camelCase.",
        func.location,
        null
    ) {
        override fun tryRaise(): Boolean {
            return !Format.isFormattedIdentifier(func.name)
        }
    }
}