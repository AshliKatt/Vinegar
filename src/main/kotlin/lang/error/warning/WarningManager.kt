package dev.ashli.vinegar.lang.error.warning

object WarningManager {
    private val warnings = mutableListOf<VinegarWarning>()

    fun generateWarning(w: VinegarWarning) {
        warnings.add(w)
    }

    fun warningCount() = warnings.size

    fun clearWarnings() {
        warnings.clear()
    }

    fun printWarnings() {
        warnings.forEach(VinegarWarning::print)
    }
}