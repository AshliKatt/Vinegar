package dev.ashli.vinegar.lang.transpiling.ast.blocks

import dev.ashli.vinegar.lang.transpiling.ast.InvertedState
import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class SelectObject(
    @SerialName("action") val type: String,
    @SerialName("inverted") val inverted: dev.ashli.vinegar.lang.transpiling.ast.InvertedState = dev.ashli.vinegar.lang.transpiling.ast.InvertedState.NORMAL,
    @SerialName("args") val arguments: dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments = dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments(),
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val block = "select_obj"
}