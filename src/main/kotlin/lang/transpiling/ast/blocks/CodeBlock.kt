package dev.ashli.vinegar.lang.transpiling.ast.blocks

import kotlinx.serialization.*
import kotlinx.serialization.json.*

/**
 * This is used for generic blocks
 */
@Serializable(with = dev.ashli.vinegar.lang.transpiling.ast.blocks.BlockSerializer::class)
sealed interface Block


// DF format is too weird for KSerializer here
object BlockSerializer : JsonContentPolymorphicSerializer<dev.ashli.vinegar.lang.transpiling.ast.blocks.Block>(dev.ashli.vinegar.lang.transpiling.ast.blocks.Block::class) {
    override fun selectDeserializer(element: JsonElement) = when (element.jsonObject["id"]?.jsonPrimitive?.content) {
        "bracket" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Bracket.serializer()
        else -> when (element.jsonObject["block"]?.jsonPrimitive?.content) {
            "call_func" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.CallFunc.serializer()
            "control" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Control.serializer()
            "else" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Else.serializer()
            "entity_action" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.EntityAction.serializer()
            "entity_event" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.EntityEvent.serializer()
            "func" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Function.serializer()
            "game_action" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.GameAction.serializer()
            "if_entity" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.IfEntity.serializer()
            "if_game" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.IfGame.serializer()
            "if_player" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.IfPlayer.serializer()
            "if_var" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.IfVar.serializer()
            "player_action" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.PlayerAction.serializer()
            "player_event" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.PlayerEvent.serializer()
            "process" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Process.serializer()
            "repeat" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.Repeat.serializer()
            "select_obj" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.SelectObject.serializer()
            "set_var" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.SetVar.serializer()
            "start_process" -> dev.ashli.vinegar.lang.transpiling.ast.blocks.StartProcess.serializer()
            else -> throw IllegalArgumentException("Input JsonObject is not a valid block")
        }
    }
}