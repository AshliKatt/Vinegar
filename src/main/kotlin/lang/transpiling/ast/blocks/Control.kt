package dev.ashli.vinegar.lang.transpiling.ast.blocks

import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Control(
    @SerialName("action") val type: String,
    @SerialName("args") val arguments: dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments = dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments(),
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val block = "control"
}