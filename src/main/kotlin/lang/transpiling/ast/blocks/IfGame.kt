package dev.ashli.vinegar.lang.transpiling.ast.blocks

import dev.ashli.vinegar.lang.transpiling.ast.InvertedState
import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class IfGame(
    @SerialName("action") val type: String,
    @EncodeDefault(EncodeDefault.Mode.NEVER) @SerialName("inverted") val inverted: dev.ashli.vinegar.lang.transpiling.ast.InvertedState = dev.ashli.vinegar.lang.transpiling.ast.InvertedState.NORMAL,
    @SerialName("args") val arguments: dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments = dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments(),
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val block = "if_game"
}