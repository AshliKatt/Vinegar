package dev.ashli.vinegar.lang.transpiling.ast.blocks

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Bracket(
    @SerialName("direct") val direction: dev.ashli.vinegar.lang.transpiling.ast.blocks.Bracket.BracketDirection,
    @SerialName("type") val type: dev.ashli.vinegar.lang.transpiling.ast.blocks.Bracket.BracketType = dev.ashli.vinegar.lang.transpiling.ast.blocks.Bracket.BracketType.NORMAL,
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "bracket"

    @Serializable
    enum class BracketDirection {
        @SerialName("open") OPEN,
        @SerialName("close") CLOSE,
    }

    @Serializable
    enum class BracketType {
        @SerialName("norm") NORMAL,
        @SerialName("repeat") REPEAT,
    }
}

