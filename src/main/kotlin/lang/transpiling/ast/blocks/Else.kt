package dev.ashli.vinegar.lang.transpiling.ast.blocks

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data object Else : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val block = "else"
}