package dev.ashli.vinegar.lang.transpiling.ast.blocks

import dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget
import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class PlayerAction(
    @SerialName("action") val type: String,
    @EncodeDefault(EncodeDefault.Mode.NEVER) @SerialName("target") val target: dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget = dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget.DEFAULT,
    @SerialName("args") val arguments: dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments = dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments(),
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val block = "player_action"
}