package dev.ashli.vinegar.lang.transpiling.ast.blocks

import dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class EntityEvent(
    @SerialName("action") val eventName: String
) : dev.ashli.vinegar.lang.transpiling.ast.blocks.Block {
    private val id = "block"
    private val args = dev.ashli.vinegar.lang.transpiling.ast.values.args.Arguments()
    private val block = "entity_event"
}