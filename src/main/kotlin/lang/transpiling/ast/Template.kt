package dev.ashli.vinegar.lang.transpiling.ast

import dev.ashli.vinegar.lang.transpiling.ast.blocks.Block
import kotlinx.serialization.Serializable

@Serializable
class Template(
    val blocks: List<dev.ashli.vinegar.lang.transpiling.ast.blocks.Block>
)