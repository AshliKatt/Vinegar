package dev.ashli.vinegar.lang.transpiling.ast

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class SelectionTarget {
    @SerialName("Selection") SELECTION,
    @SerialName("Default") DEFAULT,

    @SerialName("Killer") KILLER,
    @SerialName("Damager") DAMAGER,
    @SerialName("Victim") VICTIM,

    @SerialName("Shooter") SHOOTER,
    @SerialName("Projectile") PROJECTILE,

    @SerialName("AllPlayers") ALL_PLAYERS,
    @SerialName("AllEntities") ALL_ENTITIES,
    @SerialName("AllMobs") ALL_MOBS,

    @SerialName("LastEntity") LAST_ENTITY,
}