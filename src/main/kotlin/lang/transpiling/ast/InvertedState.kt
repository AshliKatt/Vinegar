package dev.ashli.vinegar.lang.transpiling.ast

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class InvertedState {
    @SerialName("") NORMAL,
    @SerialName("NOT") NOT
}