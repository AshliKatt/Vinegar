package dev.ashli.vinegar.lang.transpiling.ast

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class VariableScope {
    @SerialName("unsaved") GLOBAL,
    @SerialName("local") LOCAL,
    @SerialName("line") LINE,
    @SerialName("saved") SAVE,
}