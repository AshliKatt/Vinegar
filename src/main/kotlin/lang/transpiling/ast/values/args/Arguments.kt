package dev.ashli.vinegar.lang.transpiling.ast.values.args

import kotlinx.serialization.Serializable

@Serializable
class Arguments(
    val items: List<dev.ashli.vinegar.lang.transpiling.ast.values.args.Argument> = emptyList()
)