package dev.ashli.vinegar.lang.transpiling.ast.values.args

import dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Argument(
    @SerialName("slot") val slot: Int,
    @SerialName("item") val item: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value,
)