package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonClassDiscriminator

@Serializable
@Polymorphic
@OptIn(ExperimentalSerializationApi::class)
@JsonClassDiscriminator("id")
sealed class Value