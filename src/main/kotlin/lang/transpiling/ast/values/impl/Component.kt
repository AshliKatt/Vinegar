package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("comp")
class Component(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Component.ComponentData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class ComponentData(
        @SerialName("name") val name: String,
    )
}

