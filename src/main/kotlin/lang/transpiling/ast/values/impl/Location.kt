package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("loc")
class Location(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Location.LocationData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class LocationData(
        @SerialName("isBlock") val isBlock: Boolean,
        @SerialName("loc") val location: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Location.Coordinates,
    )

    @Serializable
    class Coordinates(
        val x: Double,
        val y: Double,
        val z: Double,
        val pitch: Double,
        val yaw: Double,
    )
}

