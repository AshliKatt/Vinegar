package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import dev.ashli.vinegar.lang.transpiling.ast.VariableScope
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("var")
class Variable(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Variable.VarData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class VarData(
        @SerialName("name") val name: String,
        @SerialName("scope") val scope: dev.ashli.vinegar.lang.transpiling.ast.VariableScope,
    )
}

