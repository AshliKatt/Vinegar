package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("g_val")
class GameValue(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.GameValue.ValueData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class ValueData(
        val type: String,
        val target: dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget = dev.ashli.vinegar.lang.transpiling.ast.SelectionTarget.DEFAULT,
    )
}

