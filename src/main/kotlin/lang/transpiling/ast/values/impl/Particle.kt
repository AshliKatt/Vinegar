package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.EncodeDefault.Mode.NEVER
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("part")
class Particle(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Particle.ParticleData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class ParticleData(
        @SerialName("particle") val type: String,
        @SerialName("cluster") val clusterData: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Particle.ClusterData,
        @SerialName("data") val auxData: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Particle.AuxiliaryParticleData,
    )

    @Serializable
    class ClusterData(
        val amount: Int,
        val horizontal: Double,
        val vertical: Double,
    )

    @Serializable
    @OptIn(ExperimentalSerializationApi::class)
    class AuxiliaryParticleData(
        @EncodeDefault(NEVER) val x: Double? = null,
        @EncodeDefault(NEVER) val y: Double? = null,
        @EncodeDefault(NEVER) val z: Double? = null,

        @EncodeDefault(NEVER) val material: String? = null,
        @EncodeDefault(NEVER) val size: Double? = null,
        @EncodeDefault(NEVER) val roll: Double? = null,
        @EncodeDefault(NEVER) val rgb: Int? = null,

        @EncodeDefault(NEVER) val motionVariation: Int? = null,
        @EncodeDefault(NEVER) val colorVariation: Int? = null,
        @EncodeDefault(NEVER) val sizeVariation: Int? = null,
    )
}

