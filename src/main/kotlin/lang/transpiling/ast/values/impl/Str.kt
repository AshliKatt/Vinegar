package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonClassDiscriminator

@Serializable
@SerialName("txt")
class Str(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str.StrData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class StrData(
        @SerialName("name") val name: String,
    )
}

