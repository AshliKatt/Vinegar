package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("pn_el")
class Parameter(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Parameter.ParamData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class ParamData(
        @SerialName("name") val name: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("description") val description: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("note") val note: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,

        @SerialName("type") val type: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("plural") val plural: Boolean,
        @SerialName("optional") val optional: Boolean,
    )
}

