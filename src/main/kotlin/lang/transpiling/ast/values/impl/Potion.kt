package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("pot")
class Potion(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Potion.PotionData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class PotionData(
        @SerialName("pot") val type: String,
        @SerialName("dur") val duration: Long,
        @SerialName("amp") val amplifier: Int,
    )
}

