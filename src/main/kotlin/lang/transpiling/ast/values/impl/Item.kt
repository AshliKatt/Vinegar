package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("item")
class Item(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Item.ItemData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class ItemData(
        @SerialName("item") val name: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
    )
}

