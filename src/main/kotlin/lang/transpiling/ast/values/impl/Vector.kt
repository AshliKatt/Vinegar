package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("vec")
class Vector(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Vector.VectorData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class VectorData(
        val x: Double,
        val y: Double,
        val z: Double,
    )
}

