package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("bl_tag")
class Tag(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Tag.TagData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class TagData(
        @SerialName("tag") val tagName: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("option") val tagValue: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("block") val blockType: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
        @SerialName("action") val actionType: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Str,
    )
}

