package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.EncodeDefault.Mode.NEVER
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("snd")
class Sound(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Sound.SoundData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    @OptIn(ExperimentalSerializationApi::class)
    class SoundData(
        @SerialName("pitch") val pitch: Double,
        @SerialName("vol") val volume: Double,

        @EncodeDefault(NEVER) // If empty string, do not encode it (no key selected)
        @SerialName("key") val soundKey: String? = null,

        @EncodeDefault(NEVER) // If empty string, do not encode it (no sound selected)
        @SerialName("sound") val sound: String? = null,

        @EncodeDefault(NEVER) // If empty string, do not encode it (no variant selected)
        @SerialName("variant") val variant: String? = null,
    )
}

