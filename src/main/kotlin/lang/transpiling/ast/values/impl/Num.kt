package dev.ashli.vinegar.lang.transpiling.ast.values.impl

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("num")
class Num(val data: dev.ashli.vinegar.lang.transpiling.ast.values.impl.Num.NumData) : dev.ashli.vinegar.lang.transpiling.ast.values.impl.Value() {
    @Serializable
    class NumData(
        @SerialName("name") val name: String,
    )
}

