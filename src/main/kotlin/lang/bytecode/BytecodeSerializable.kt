package dev.ashli.vinegar.lang.bytecode

import java.io.DataOutputStream

/**
 * Represents that a type can be written to a DataOutputStream
 */
interface BytecodeSerializable {

    fun writeTo(stream: DataOutputStream)
}

/**
 * Writes this string to a DataOutputStream
 */
fun String.writeTo(stream: DataOutputStream) {
    val byteArray = this.encodeToByteArray()
    stream.write(byteArray.size)
    stream.write(byteArray)
}

/**
 * Writes this list to a DataOutputStream. Use a ByteArray for lists primitive types.
 */
fun <T : BytecodeSerializable> Collection<T>.writeTo(stream: DataOutputStream) {
    stream.write(this.size)
    this.forEach {
        it.writeTo(stream)
    }
}

/**
 * Writes this list to a DataOutputStream. Use a ByteArray for lists primitive types.
 */
fun <T> List<T>.writeTo(stream: DataOutputStream, map: (T) -> BytecodeSerializable) {
    stream.write(this.size)
    this.forEach {
        map(it).writeTo(stream)
    }
}