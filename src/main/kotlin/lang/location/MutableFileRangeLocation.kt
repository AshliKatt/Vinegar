package dev.ashli.vinegar.lang.location

import java.io.File

class MutableFileRangeLocation(var line: Int, var col: Int, var lineEnd: Int, var colEnd: Int, val file: File) {
    companion object {
        fun between(a: FileLocation, b: FileLocation) : MutableFileRangeLocation {
            if (a.file != b.file) throw IllegalArgumentException("Inputs must point to the same file")
            return MutableFileRangeLocation(a.line, a.col, b.line, b.col, a.file)
        }
    }

    fun immutable() = FileRangeLocation(line, col, lineEnd, colEnd, file)

    fun point() = MutableFileLocation(line, col, file)
    fun endPoint() = MutableFileLocation(lineEnd, colEnd, file)
}