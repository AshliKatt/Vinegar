package dev.ashli.vinegar.lang.location

import java.io.File

data class FileRangeLocation(val line: Int, val col: Int, val lineEnd: Int, val colEnd: Int, val file: File) {
    companion object {
        fun between(a: FileLocation, b: FileLocation) : FileRangeLocation {
            if (a.file != b.file) throw IllegalArgumentException("Inputs must point to the same file")
            return FileRangeLocation(a.line, a.col, b.line, b.col, a.file)
        }
    }

    fun mutable() = MutableFileRangeLocation(line, col, lineEnd, colEnd, file)

    fun point() = FileLocation(line, col, file)
    fun endPoint() = FileLocation(lineEnd, colEnd, file)

    fun widen(amount: Int = 1) = FileRangeLocation(line, col, lineEnd, colEnd + amount, file)
    fun to(other: FileLocation) = between(this.point(), other)
    fun to(other: FileRangeLocation) = between(this.point(), other.endPoint())
}