package dev.ashli.vinegar.lang.location

import java.io.File

data class FileLocation(val line: Int, val col: Int, val file: File) {
    fun mutable() = MutableFileLocation(line, col, file)

    fun range(size: Int = 1) = FileRangeLocation(line, col, line, col + size, file)

    fun add(line: Int, col: Int) = FileLocation(line + this.line, col + this.col, file)

    fun to(other: FileLocation) = FileRangeLocation.between(this, other)
    fun to(other: FileRangeLocation) = FileRangeLocation.between(this, other.endPoint())
}