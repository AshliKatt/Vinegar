package dev.ashli.vinegar.lang.location

/**
 * Represents the immutable location of a program resource.
 */
open class ResourceLocation(internal val path: MutableList<String> = mutableListOf()) {
    override fun toString() = path.joinToString("::")

    fun with(location: String): ResourceLocation {
        val clone = path.toMutableList()
        clone.add(location)
        return ResourceLocation(clone)
    }

    fun with(location: ResourceLocation): ResourceLocation {
        val clone = path.toMutableList()
        clone.addAll(location.path)
        return ResourceLocation(clone)
    }

    fun mutable() = MutableResourceLocation(path)

    override fun equals(other: Any?): Boolean {
        if (other !is ResourceLocation) return false

        return this.toString() == other.toString()
    }

    override fun hashCode() = this.toString().hashCode()
}