package dev.ashli.vinegar.lang.location

/**
 * Represents a mutable location of a program resource.
 */
class MutableResourceLocationPattern internal constructor(
    path: MutableList<String> = mutableListOf(),
    starPattern: Boolean
) : ResourceLocationPattern(path, starPattern) {
    fun append(location: ResourceLocationPattern) {
        path.addAll(location.path)
    }

    fun append(location: String) {
        path.add(location)
    }

    fun toParent() {
        path.removeLast()
    }

    fun hasParent() = path.size > 1

    fun immutable() = ResourceLocationPattern(path.toMutableList(), starPattern)

}