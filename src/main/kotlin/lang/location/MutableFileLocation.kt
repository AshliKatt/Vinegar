package dev.ashli.vinegar.lang.location

import java.io.File

class MutableFileLocation(var line: Int, var col: Int, val file: File) {
    fun immutable() = FileLocation(line, col, file)

    fun range(size: Int = 1) = MutableFileRangeLocation(line, col, line, col + size, file)

    fun add(line: Int, col: Int) {
        this.line += line
        this.col += col
    }
}