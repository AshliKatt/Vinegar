package dev.ashli.vinegar.lang.location

/**
 * Represents a mutable location of a program resource.
 */
class MutableResourceLocation internal constructor(
    path: MutableList<String> = mutableListOf()
) : ResourceLocation(path) {
    override fun toString() = path.joinToString("::")

    fun append(location: ResourceLocation) {
        path.addAll(location.path)
    }

    fun append(location: String) {
        path.add(location)
    }

    fun toParent() {
        path.removeLast()
    }

    fun hasParent() = path.size > 1

    fun immutable() = ResourceLocation(path.toMutableList())

}