package dev.ashli.vinegar.lang.location

/**
 * Represents the immutable pattern to match resource locations from.
 */
open class ResourceLocationPattern(
    internal val path: MutableList<String> = mutableListOf(),
    internal val starPattern: Boolean
) {
    override fun toString() = path.joinToString("::") + if (starPattern) "::*" else ""

    fun with(location: String): ResourceLocationPattern {
        val clone = path.toMutableList()
        clone.add(location)
        return ResourceLocationPattern(clone, starPattern)
    }

    fun with(location: ResourceLocationPattern): ResourceLocationPattern {
        val clone = path.toMutableList()
        clone.addAll(location.path)
        return ResourceLocationPattern(clone, starPattern)
    }

    fun mutable() = MutableResourceLocationPattern(path, starPattern)

    fun matches(location: ResourceLocation) {

    }

    override fun hashCode() = this.toString().hashCode()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResourceLocationPattern

        return path == other.path
    }
}