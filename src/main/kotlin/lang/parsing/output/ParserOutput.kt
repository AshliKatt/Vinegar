package dev.ashli.vinegar.lang.parsing.output

import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.tree.ParseTree

/**
 * Holds all relevant Parse Trees after parsing.
 */
class ParserOutput(val trees: List<ParseTree>, val imports: List<ResourceLocation>) {
    override fun toString() = "${imports.joinToString(", ")}\n\n${trees.joinToString("\n")}"
}