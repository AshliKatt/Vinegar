package dev.ashli.vinegar.lang.parsing.modifiers

import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifier.entries

/**
 * EnumSet-like container for top level modifiers.
 */
class TopLevelModifiers {
    private var value = 0L

    fun add(modifier: TopLevelModifier) {
        value = value or modifier.enumCode()
    }

    fun has(modifier: TopLevelModifier): Boolean {
        return value and modifier.enumCode() > 0L
    }

    fun remove(modifier: TopLevelModifier) {
        value = value and modifier.enumCode().inv()
    }

    fun hasNum(vararg modifiers: TopLevelModifier) = modifiers.count { has(it) }

    fun isEmpty() = value == 0L

    override fun toString() = if (isEmpty()) "no modifiers" else entries.filter(::has).joinToString { "$it " }
}