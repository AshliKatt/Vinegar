package dev.ashli.vinegar.lang.parsing.modifiers

import dev.ashli.vinegar.lang.tokenization.token.TokenType

/**
 * Represents modifiers for top-level structures
 */
enum class TopLevelModifier(private val type: TokenType, private val representation: String) {

    // VISIBILITY
    PUBLIC(TokenType.Public, "public"), // Accessible everywhere (Default)
    INTERNAL(TokenType.Internal, "internal"), // Private to this namespace
    PRIVATE(TokenType.Private, "private"), // Private to this file
    ;

    fun enumCode(): Long {
        return 1L shl ordinal
    }

    companion object {
        /**
         * Returns this token type as a top level modifier, or null if it's not a modifier token type
         * @param token The token type to check
         */
        fun getModifier(token: TokenType) = TopLevelModifier.entries.firstOrNull { it.type == token }
    }

    override fun toString() = representation
}