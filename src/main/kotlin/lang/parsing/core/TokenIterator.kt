package dev.ashli.vinegar.lang.parsing.core

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.parsing.error.ParsingError
import dev.ashli.vinegar.lang.parsing.error.RangeParsingError
import dev.ashli.vinegar.lang.tokenization.token.Token
import dev.ashli.vinegar.lang.tokenization.token.TokenType
import java.io.File

/**
 * Handles simple methods for progressing a token iterator, as well as convenience methods for auto-checking
 * token values and throwing appropriate errors.
 */
open class TokenIterator(file: File, private val tokens: List<Token>) {
    private var index = 0

    /**
     * The location of the last-consumed token
     */
    internal var loc = FileRangeLocation(0, 0, 0, 0, file)

    /**
     * Returns the next token without progressing the iterator. Undefined behavior when explicitHasNext() is false.
     */
    internal fun explicitPeek() = tokens[index]

    /**
     * Returns the next token and progresses the iterator. Undefined behavior when explicitHasNext() is false.
     */
    internal fun explicitNext(): Token {
        val out = tokens[index++]
        loc = out.loc
        return out
    }

    /**
     * Returns if there are more tokens to iterate over.
     */
    internal fun explicitHasNext() = index < tokens.size

    /**
     * Skips over the next token.
     */
    internal fun explicitSkip() {
        loc = tokens[index++].loc
    }

    /**
     * Skips all newlines and then peek the next token without progressing the iterator.
     * @param expected A short message listing the expected tokens, example: "open or close parenthesis."
     * @throws ParsingError If no non-newline tokens remain.
     */
    internal fun peek(expected: String): Token {
        skipNewlines()
        return tokens.getOrNull(index) ?: throw ParsingError.ExpectedTokenError(expected, loc.endPoint())
    }

    /**
     * Skips all newlines and then returns the next token, progressing the iterator.
     * @param expected A short message listing the expected tokens, example: "open or close parenthesis."
     * @throws ParsingError If no non-newline tokens remain.
     */
    internal fun next(expected: String): Token {
        skipNewlines()
        val out = tokens.getOrNull(index++) ?: throw ParsingError.ExpectedTokenError(expected, loc.endPoint())
        loc = out.loc
        return out
    }

    /**
     * Skips all newlines and then returns whether more tokens remain.
     */
    internal fun hasNext(): Boolean {
        skipNewlines()
        return explicitHasNext()
    }

    /**
     * Returns if there are more non-newline tokens in this iterator without modifying its state.
     */
    internal fun scanHasNext(): Boolean {
        var i = index
        while (i < tokens.size && tokens[i].matches(TokenType.Newline)) i++
        return i < tokens.size
    }

    /**
     * Returns the next non-newline token without progressing the iterator. Undefined behavior when none remain.
     */
    internal fun scanPeek(): Token {
        var i = index
        while (i < tokens.size && tokens[i].matches(TokenType.Newline)) i++
        return tokens[i]
    }

    /**
     * Returns the next non-newline token without progressing the iterator. Errors when none remain.
     */
    internal fun scanCheckedPeek(expected: String): Token {
        var i = index
        while (i < tokens.size && tokens[i].matches(TokenType.Newline)) i++
        return tokens.getOrNull(i) ?: throw ParsingError.ExpectedTokenError(expected, loc.endPoint())
    }

    /**
     * Skips all newlines and then skips over the next token.
     */
    internal fun skip() {
        skipNewlines()
        loc = tokens[index++].loc
    }

    /**
     * Skips all newlines and then peek the next token without progressing the iterator.
     * Undefined behavior if no non-newline tokes remain.
     */
    internal fun uncheckedPeek(): Token {
        skipNewlines()
        return tokens[index]
    }


    /**
     * Skips all newlines and then returns the next token, progressing the iterator.
     * Undefined behavior if no non-newline tokens remain.
     */
    internal fun uncheckedNext(): Token {
        skipNewlines()
        val out = tokens[index++]
        loc = out.loc
        return out
    }

    /**
     * Skips all newlines and returns the next token with extra checks.
     * @param msg A short message listing the expected tokens, example: "open or close parenthesis."
     * @param allowed Permitted TokenTypes.
     * @throws ParsingError If the iterator has no non-newline tokens or token doesn't match the input types.
     */
    internal fun expectNext(msg: String, vararg allowed: TokenType): Token {
        val token = next(msg)
        if (allowed.none(token.type::matches)) {
            throw RangeParsingError.UnexpectedTokenError(msg, loc)
        }
        return token
    }

    /**
     * Runs a mapper function while the next token matches a pattern.
     * This method itself will not consume the "next" token.
     * Returned value from the mapper function will be collected into a list.
     * @param allowed Permitted TokenTypes.
     * @return List of parsed elements defined by the mapper function.
     */
    internal fun <T> collect(vararg allowed: TokenType, mapper: () -> T): MutableList<T> {
        val out = mutableListOf<T>()

        while (scanHasNext() && scanPeek().matches(*allowed)) {
            out.add(mapper())
        }

        return out
    }

    /**
     * Runs a mapper function while the next token does not match a pattern.
     * This method itself will not consume the "next" token.
     * Returned value from the mapper function will be collected into a list.
     * @param allowed Permitted TokenTypes.
     * @return List of parsed elements defined by the mapper function.
     */
    internal fun <T> collectUntil(vararg allowed: TokenType, mapper: () -> T): List<T> {
        val out = mutableListOf<T>()

        while (scanHasNext() && !scanPeek().matches(*allowed)) {
            out.add(mapper())
        }

        return out
    }

    /**
     * Runs if the next token matches a pattern. Newline-safe.
     * @param allowed Permitted TokenTypes.
     * @return True if there are more tokens in this iterator and the next one matches the pattern.
     */
    internal fun peekIs(vararg allowed: TokenType): Boolean {
        return scanHasNext() && scanPeek().matches(*allowed)
    }

    /**
     * Runs if the next token does not match a pattern. Newline-safe.
     * @param allowed Permitted TokenTypes.
     * @return True if there are no more tokens in this iterator or if the next one does not match the pattern.
     */
    internal fun peekIsNot(vararg allowed: TokenType): Boolean {
        return !scanHasNext() || !scanPeek().matches(*allowed)
    }

    /**
     * Runs input code 0 or more times, checking for a separator between each call and a delimiter to end.
     * Will consume all separators, but will not consume the delimiter.
     * @param separator The separator token to use (such as COMMA)
     * @param delimiter The delimiter token to use (such as a closing bracket or paren)
     * @return A list of all collected values
     */
    internal fun <T> parseMultipleDelimited(separator: TokenType, delimiter: TokenType, consumer: () -> T): List<T> {
        val out = mutableListOf<T>()

        while (peekIsNot(delimiter)) {
            out.add(consumer())

            if (peekIs(separator)) {
                skip()
            } else {
                break
            }
        }

        return out
    }

    /**
     * Continuously skips the next token so long as it is a newline.
     */
    private fun skipNewlines() {
        while (explicitHasNext() && explicitPeek().matches(TokenType.Newline)) {
            explicitNext()
        }
    }
}