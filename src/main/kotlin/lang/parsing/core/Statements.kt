package dev.ashli.vinegar.lang.parsing.core

import dev.ashli.vinegar.helper.applyIf
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.error.RangeParsingError
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifiers
import dev.ashli.vinegar.lang.parsing.tree.*
import dev.ashli.vinegar.lang.tokenization.token.TokenType

// statementEnd = ? end of file ? | SEMICOLON | NEWLINE
internal fun parseStatementEnd(iter: TokenIterator) {
    if (!iter.explicitHasNext()) return // EOF is valid statement end
    if (!iter.explicitNext().matches(TokenType.Semicolon, TokenType.Newline)) {
        throw RangeParsingError.UnexpectedTokenError("semicolon or newline", iter.loc)
    }
}

// codeblock = statement | '{', { statement }, '}'
internal fun parseCodeBlock(iter: TokenIterator, forceBrackets: Boolean): Statement {
    if (forceBrackets || iter.peek("statement").matches(TokenType.OpenBrace)) {
        iter.expectNext("codeblock", TokenType.OpenBrace) // Skip {
        val startLocation = iter.loc

        val statements = iter.collectUntil(TokenType.CloseBrace) {
            parseStatement(iter)
        }

        iter.expectNext("end of codeblock", TokenType.CloseBrace)

        return BlockStatementNode(startLocation.to(iter.loc), statements)
    } else {
        return parseStatement(iter)
    }
}

/*
    statement =
    expression, statementEnd |
    LET, IDENTIFIER, [COLON, resourceLocation], EQUALS, expression, statementEnd |
    LET, IDENTIFIER, COLON, resourceLocation, [EQUALS, expression], statementEnd |
    CONST, IDENTIFIER, [COLON resourceLocation], EQUALS expression statementEnd |
    IF, condition, statement [ELSE, statement] |
    WHILE, condition, statement |
    FOR, OPEN_PAREN, IDENTIFIER, IN, expression, CLOSE_PAREN, statement |
    BREAK, statementEnd |
    CONTINUE, statementEnd |
    RETURN, [expression], statementEnd
 */
internal fun parseStatement(iter: TokenIterator): Statement {
    return when (iter.peek("statement").type) {
        TokenType.Let -> parseLet(iter, TopLevelModifiers(), emptyList())
        TokenType.Const -> parseConst(iter, TopLevelModifiers(), emptyList())
        TokenType.If -> {
            iter.explicitSkip() // Skip "if"
            val location = iter.loc

            val expr = parseCondition(iter, "if")

            val statement = parseCodeBlock(iter, false) // Main branch

            if (iter.hasNext() && iter.uncheckedPeek().matches(TokenType.Else)) { // If there is an else?
                IfElseStatementNode(location, expr, statement, parseCodeBlock(iter, false))
            } else {
                IfStatementNode(location, expr, statement)
            }
        }
        TokenType.While -> {
            iter.explicitSkip() // Skip "while"
            val location = iter.loc

            val expr = parseCondition(iter, "while-loop")
            WhileStatementNode(location, expr, parseCodeBlock(iter, false))
        }
        TokenType.For -> {
            iter.explicitSkip() // Skip "for"
            val location = iter.loc

            iter.expectNext("for-loop expression", TokenType.OpenParen) // Skip "("
            val constName = parseName(iter, "iteration variable name")
            val typeHint = maybeParseTypeHint(iter)
            iter.expectNext("'in' to continue for-loop expression", TokenType.In) // Skip "in"
            val iterValue = parseExpression(iter) // Iter
            iter.expectNext("end of for-loop expression", TokenType.CloseParen) // Skip ")"
            val body = parseCodeBlock(iter, false)


            ForStatementNode(location, constName, typeHint, iterValue, body)
        }
        TokenType.Break -> {
            iter.explicitSkip() // skip "break"
            val location = iter.loc

            parseStatementEnd(iter) // ; or newline
            BreakStatementNode(location)
        }
        TokenType.Continue -> {
            iter.explicitSkip() // skip "continue"
            val location = iter.loc

            parseStatementEnd(iter) // ; or newline
            ContinueStatementNode(location)
        }
        TokenType.Return -> {
            iter.explicitSkip() // Skip return
            val location = iter.loc

            if (iter.explicitHasNext() && !iter.explicitPeek().matches(TokenType.Newline, TokenType.Semicolon)) {
                val e = parseExpression(iter)
                parseStatementEnd(iter)
                ReturnStatementNode(location, e)
            } else {
                parseStatementEnd(iter)
                ReturnStatementNode(location, null)
            }
        }
        else -> {
            val expr = parseExpression(iter)
            parseStatementEnd(iter) // ; or newline
            ExprStatementNode(expr.location, expr)
        }
    }
}

// condition = [EXCLAMATION], '(', expression, ')'
internal fun parseCondition(iter: TokenIterator, type: String): Expression {
    val inverse = iter.peekIs(TokenType.Exclaim)
    if (inverse) iter.skip()

    iter.expectNext("$type condition", TokenType.OpenParen) // Skip "("

    val expr = parseExpression(iter).applyIf(inverse) {
        BoolNotNode(it.location, it)
    }

    iter.expectNext("end of $type condition", TokenType.CloseParen) // Skip ")"
    return expr
}

/**
 * Parses a type hint if one is present. Returns the referenced resource.
 */
internal fun maybeParseTypeHint(iter: TokenIterator): ResourceLocation? {
    return if (iter.peekIs(TokenType.Colon)) {
        iter.skip()
        parseResourceLocation(iter, "datatype hint")
    } else null
}