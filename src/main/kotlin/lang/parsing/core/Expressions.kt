package dev.ashli.vinegar.lang.parsing.core

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.tree.*
import dev.ashli.vinegar.lang.tokenization.token.TokenType

private typealias Combinator = (FileRangeLocation, Expression, Expression) -> Expression

private fun binaryOperationMap(token: TokenType): Combinator? {
    return when (token) {
        TokenType.Asterisk -> ::MultiplyNode
        TokenType.BoolAnd -> ::BoolAndNode
        TokenType.BoolOr -> ::BoolOrNode
        TokenType.Equals -> ::EqualNode
        TokenType.GreaterEquals -> ::GreaterEqNode
        TokenType.GreaterThan -> ::GreaterNode
        TokenType.LessEquals -> ::LessEqNode
        TokenType.LessThan -> ::LessNode
        TokenType.Minus -> ::MinusNode
        TokenType.NotEquals -> ::NotEqualNode
        TokenType.Percent -> ::ModuloNode
        TokenType.Plus -> ::PlusNode
        TokenType.Slash -> ::DivideNode
        else -> null
    }
}

private inline fun ltr(iter: TokenIterator, parseNext: (TokenIterator) -> Expression, vararg tokens: TokenType): Expression {
    var value = parseNext(iter)

    while (iter.scanHasNext() && iter.scanPeek().matches(*tokens)) {
        val type = iter.uncheckedNext().type
        value = binaryOperationMap(type)!!(iter.loc, value, parseNext(iter))
    }

    return value
}

/**
 * Parses an expression.
 *
 * Precedence:
 * VALUE
 * LTR a\[b] (x) a.b
 * RTL -x !x UNARY
 * LTR a*b a/b a%b TERMS
 * LTR a+b a-b FACTORS
 * LTR a<b a<=b a>b a>=b COMPARISON
 * LTR a==b a!=b EQUALITY
 * LTR && AND
 * LTR || OR
 */
internal fun parseExpression(iter: TokenIterator) = parseOr(iter)
internal fun parseOr(iter: TokenIterator) = ltr(iter, ::parseAnd, TokenType.BoolOr)
internal fun parseAnd(iter: TokenIterator) = ltr(iter, ::parseEquality, TokenType.BoolAnd)
internal fun parseEquality(iter: TokenIterator) = ltr(iter, ::parseComparison, TokenType.Equals, TokenType.NotEquals)
internal fun parseComparison(iter: TokenIterator) = ltr(iter, ::parseFactors, TokenType.GreaterEquals, TokenType. GreaterThan,
    TokenType.LessEquals, TokenType.LessThan)
internal fun parseFactors(iter: TokenIterator) = ltr(iter, ::parseTerms, TokenType.Plus, TokenType.Minus)
internal fun parseTerms(iter: TokenIterator) = ltr(iter, ::parseUnary, TokenType.Asterisk, TokenType.Slash, TokenType.Percent)
internal fun parseUnary(iter: TokenIterator): Expression {
    return when (iter.scanCheckedPeek("value").type) {
        TokenType.Exclaim -> {
            iter.uncheckedNext();
            BoolNotNode(iter.loc, parseUnary(iter))
        }
        TokenType.Minus -> {
            iter.uncheckedNext();
            NegateNode(iter.loc, parseUnary(iter))
        }
        else -> parseIndex(iter)
    }
}
internal fun parseIndex(iter: TokenIterator): Expression {
    var out = parseValue(iter)

    while (iter.scanHasNext()) {
        val nextToken = iter.scanPeek()
        if (nextToken.matches(TokenType.OpenBracket)) { // a[b]
            val beginLoc = iter.loc.point()
            iter.skip()
            val index = parseExpression(iter)
            val endLoc = iter.loc.endPoint()
            out = IndexNode(FileRangeLocation.between(beginLoc, endLoc), out, index)

        } else if (nextToken.matches(TokenType.Period)) { // a.b
            iter.skip()
            val name = parseName(iter, "property or method name")

            if (iter.peekIs(TokenType.OpenParen)) {
                val loc = iter.loc
                iter.skip() // Skip (

                val args = iter.parseMultipleDelimited(TokenType.Comma, TokenType.CloseParen) {
                    parseExpression(iter)
                }

                iter.expectNext("end of argument list", TokenType.CloseParen) // Skip )

                out = MethodCallNode(loc, out, name, args)
            } else {
                out = PropertyAccessNode(iter.loc, out, name)
            }
        } else {
            break
        }
    }

    return out
}
internal fun parseValue(iter: TokenIterator): Expression {
    return when (val x = iter.peek("value").type) {
        is TokenType.Float -> {
            iter.explicitSkip()
            FloatNode(iter.loc, x.value)
        }
        is TokenType.Int -> {
            iter.explicitSkip()
            IntNode(iter.loc, x.value)
        }
        is TokenType.Str -> {
            iter.explicitSkip()
            StrNode(iter.loc, x.value)
        }
        is TokenType.Text -> {
            iter.explicitSkip()
            TextNode(iter.loc, x.value)
        }
        TokenType.True -> {
            iter.explicitSkip()
            BoolNode(iter.loc, true)
        }
        TokenType.False -> {
            iter.explicitSkip()
            BoolNode(iter.loc, false)
        }
        TokenType.OpenParen -> {
            iter.explicitSkip()
            val out = parseExpression(iter)
            iter.expectNext("closing parenthesis", TokenType.CloseParen)
            out
        }
        else -> {
            // Either a var reference or a func call
            val resource = parseResourceLocation(iter, "value")
            val location = iter.loc

            if (iter.peekIs(TokenType.OpenParen)) { // Func call
                iter.skip() // Skip (

                val args = iter.parseMultipleDelimited(TokenType.Comma, TokenType.CloseParen) {
                    parseExpression(iter)
                }

                iter.expectNext("end of argument list", TokenType.CloseParen) // Skip )

                FunctionCallNode(location, resource, args)
            } else { // Variable
                ResourceLocationNode(location, resource)
            }
        }
    }
}

// resourceLocation = IDENTIFIER, { '::', IDENTIFIER }
internal fun parseResourceLocation(iter: TokenIterator, expected: String): ResourceLocation {
    val value = mutableListOf(parseName(iter, expected))

    while (iter.scanHasNext() && iter.scanPeek().matches(TokenType.DoubleColon)) {
        iter.skip() // Skip ::
        value.add(parseName(iter, expected))
    }

    return ResourceLocation(value)
}