package dev.ashli.vinegar.lang.parsing.core

import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.error.RangeParsingError
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifier
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifiers
import dev.ashli.vinegar.lang.parsing.output.ParserOutput
import dev.ashli.vinegar.lang.parsing.tree.ParseTree
import dev.ashli.vinegar.lang.parsing.tree.Annotation
import dev.ashli.vinegar.lang.tokenization.token.Token
import dev.ashli.vinegar.lang.tokenization.token.TokenType
import java.io.File

/**
 * Parses a token iterator into a ParseTree
 */
fun parse(tokens: List<Token>, file: File) = parseTopLevel(TokenIterator(file, tokens))

// topLevel ::= { import }, { topLevelDeclaration }
internal fun parseTopLevel(iter: TokenIterator): ParserOutput {
    val out = mutableListOf<ParseTree>()

    val imports = iter.collect(TokenType.Import) {
        parseImport(iter)
    }

    while (iter.hasNext()) {
        out.add(parseTopLevelDeclaration(iter))
    }

    return ParserOutput(out, imports)
}

// topLevelDeclaration ::= namespace
internal fun parseTopLevelDeclaration(iter: TokenIterator): ParseTree {
    val annotations = parseAnnotations(iter)
    val modifiers = parseModifiers(iter)
    val token = iter.peek("function or namespace declaration")

    if (token.matches(TokenType.Namespace)) {
        return parseNamespace(iter, modifiers, annotations)
    }

    // All errors below this point
    iter.explicitSkip()

    when (token.type) {
        TokenType.Let -> throw RangeParsingError.NoTopLevelError("Variable declarations", iter.loc)
        TokenType.Const -> throw RangeParsingError.NoTopLevelError("Constant declarations", iter.loc)
        TokenType.Function -> throw RangeParsingError.NoTopLevelError("Functions", iter.loc)
        TokenType.Import -> throw RangeParsingError.MisplacedImportError(iter.loc)
        else -> throw RangeParsingError.UnexpectedTokenError("top-level element", iter.loc)
    }
}

// declaration = annotations, modifiers, (namespace | let | const | function)
internal fun parseDeclaration(iter: TokenIterator): ParseTree {
    val annotations = parseAnnotations(iter)
    val modifiers = parseModifiers(iter)
    val token = iter.peek("function or namespace declaration")

    when (token.type) {
        TokenType.Namespace -> return parseNamespace(iter, modifiers, annotations)
        TokenType.Let -> return parseLet(iter, modifiers, annotations)
        TokenType.Const -> return parseConst(iter, modifiers, annotations)
        TokenType.Function -> return parseFunction(iter, modifiers, annotations)
        else -> {}
    }

    iter.explicitSkip()

    if (token.matches(TokenType.Import)) {
        throw RangeParsingError.MisplacedImportError(iter.loc)
    }

    throw RangeParsingError.UnexpectedTokenError("top-level element", iter.loc)
}

// import = IMPORT, resourceLocation, statementEnd
private fun parseImport(iter: TokenIterator): ResourceLocation {
    iter.skip() // Skip import
    val namespace = parseResourceLocation(iter, "namespace to use") // newline non-destructive
    parseStatementEnd(iter) // ; or newline
    return namespace
}

// modifiers = { MODIFIER_TOKEN }
internal fun parseModifiers(iter: TokenIterator): TopLevelModifiers {
    val out = TopLevelModifiers()
    while (iter.hasNext() && TopLevelModifier.getModifier(iter.explicitPeek().type) != null) {
        val token = iter.explicitNext()
        val modifier = TopLevelModifier.getModifier(token.type)!!

        if (out.has(modifier)) {
            throw RangeParsingError.DuplicateModifiersError(iter.loc)
        }

        out.add(modifier)

        if (out.hasNum(TopLevelModifier.PUBLIC, TopLevelModifier.PRIVATE, TopLevelModifier.INTERNAL) > 1) {
            throw RangeParsingError.IncompatibleVisibilityModifiersError(iter.loc)
        }
    }
    return out
}

// annotations = { ANNOTATE, IDENTIFIER, ['(', [expression, { ',', expression }, [',']], ')' ] }
internal fun parseAnnotations(iter: TokenIterator): List<Annotation> {
    return iter.collect(TokenType.Annotate) {
        val beginning = iter.uncheckedNext().loc.point()
        val name = parseResourceLocation(iter, "annotation name")

        if (iter.peekIs(TokenType.OpenParen)) {
            iter.skip()

            val args = iter.parseMultipleDelimited(TokenType.Comma, TokenType.CloseParen) {
                parseExpression(iter)
            }

            iter.expectNext("end of annotation argument list", TokenType.CloseParen)

            Annotation(beginning.to(iter.loc), name, args)
        } else {
            Annotation(beginning.to(iter.loc), name, emptyList())
        }
    }
}

/**
 * Parses the next identifier and returns its name value. Throws errors as appropriate.
 */
internal fun parseName(iter: TokenIterator, expected: String): String {
    val nameToken = iter.next(expected)
    return if (nameToken.type is TokenType.Identifier) {
        nameToken.type.value
    } else {
        throw RangeParsingError.UnexpectedTokenError(expected, iter.loc)
    }
}