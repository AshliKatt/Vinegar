package dev.ashli.vinegar.lang.parsing.core

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.parsing.error.ParsingError
import dev.ashli.vinegar.lang.parsing.error.RangeParsingError
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifiers
import dev.ashli.vinegar.lang.parsing.tree.*
import dev.ashli.vinegar.lang.parsing.tree.Annotation
import dev.ashli.vinegar.lang.tokenization.token.TokenType


// namespace = NAMESPACE, resourceLocation, (statementEnd | '{', {topLevel}, '}')
internal fun parseNamespace(iter: TokenIterator, modifiers: TopLevelModifiers, annotations: List<Annotation>): ParseTree {
    val tokenLoc = iter.explicitNext().loc // Skip namespace

    // Check legality
    if (!modifiers.isEmpty()) {
        throw RangeParsingError.NamespaceModifierError(iter.loc)
    }

    if (annotations.isNotEmpty()) {
        throw RangeParsingError.NamespaceAnnotationError(iter.loc)
    }

    val namespace = parseResourceLocation(iter, "namespace name")
    val inside = mutableListOf<ParseTree>()

    if (iter.scanHasNext() && iter.scanPeek().matches(TokenType.OpenBrace)) { // Next thing is {
        iter.skip() // Skip {

        while (iter.hasNext() && !iter.explicitPeek().matches(TokenType.CloseBrace)) {
            inside.add(parseDeclaration(iter))
        }

        iter.expectNext("end of namespace body", TokenType.CloseBrace)
    } else { // Bodiless namespace
        parseStatementEnd(iter)
        while (iter.hasNext()) {
            inside.add(parseDeclaration(iter))
        }
    }

    return NamespaceDefNode(tokenLoc, namespace, inside)
}

// function = FN, IDENTIFIER, '(', { parameter, ',' }, ')', [COLON, resourceLocation], codeblock
internal fun parseFunction(iter: TokenIterator, modifiers: TopLevelModifiers, annotations: List<Annotation>): ParseTree {
    iter.explicitSkip() // Skip fn

    val name = parseName(iter, "function name")
    val nameLocation = iter.loc
    iter.expectNext("function parameter list", TokenType.OpenParen)

    val params = mutableListOf<Parameter>()
    while (iter.scanHasNext() && !iter.scanPeek().matches(TokenType.CloseParen)) {
        val paramAnnotations = parseAnnotations(iter)
        val paramName = parseName(iter, "parameter name")
        val beginRange = iter.loc
        val paramType = maybeParseTypeHint(iter) ?: throw ParsingError.RequiredParamTypeError(iter.loc.endPoint())
        val endRange = iter.loc
        params.add(
            Parameter(
                FileRangeLocation.between(beginRange.point(), endRange.endPoint()),
                paramAnnotations,
                paramName,
                paramType
            )
        )

        if (iter.scanHasNext() && iter.scanPeek().matches(TokenType.Comma)) {
            iter.skip() // Skip ,
        } else {
            break // No comma, we're done
        }
    }

    iter.expectNext("end of function parameter list", TokenType.CloseParen) // Skip )

    val returnType = maybeParseTypeHint(iter)
    val body = parseCodeBlock(iter, true)

    return FunctionDefNode(nameLocation, annotations, modifiers, name, params, returnType, body)
}

// let = LET, IDENTIFIER, [COLON, resourceLocation], EQUALS, expression, statementEnd |
// LET, IDENTIFIER, COLON, resourceLocation, [EQUALS, expression], statementEnd
internal fun parseLet(iter: TokenIterator, modifiers: TopLevelModifiers, annotations: List<Annotation>): VarDeclarationNode {
    iter.explicitSkip() // Skip "let"
    val varName = parseName(iter, "variable name")
    val varLocation = iter.loc
    val typeHint = maybeParseTypeHint(iter)

    return if (iter.peekIs(TokenType.Assign)) {
        iter.explicitSkip() // Skip =
        val value = parseExpression(iter)
        parseStatementEnd(iter)
        VarDeclarationNode(varLocation, annotations, modifiers, varName, typeHint, value)
    } else if (typeHint != null) {
        parseStatementEnd(iter)
        VarDeclarationNode(varLocation, annotations, modifiers, varName, typeHint, null)
    } else {
        throw ParsingError.RequiredVarInitializationError(iter.loc.endPoint())
    }
}

// CONST, IDENTIFIER, [COLON resourceLocation], EQUALS expression statementEnd
internal fun parseConst(iter: TokenIterator, modifiers: TopLevelModifiers, annotations: List<Annotation>): ConstDeclarationNode {
    iter.explicitSkip() // Skip "const"
    val constName = parseName(iter, "constant name")
    val constLocation = iter.loc
    val typeHint = maybeParseTypeHint(iter)

    if (iter.peekIsNot(TokenType.Assign)) {
        throw ParsingError.RequiredConstInitializationError(iter.loc.endPoint())
    }

    iter.explicitSkip() // Skip =
    val value = parseExpression(iter)
    parseStatementEnd(iter)
    return ConstDeclarationNode(constLocation, annotations, modifiers, constName, typeHint, value)
}