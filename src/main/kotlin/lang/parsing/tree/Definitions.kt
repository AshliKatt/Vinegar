package dev.ashli.vinegar.lang.parsing.tree

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifiers

class Parameter(
    loc: FileRangeLocation,
    val annotations: List<Annotation>,
    val name: String,
    val type: ResourceLocation
): ParseTree(loc) {
    override fun getChildren() = annotations
    override fun prettyString() = "Parameter($name, $type)"
}

class Annotation(loc: FileRangeLocation, val name: ResourceLocation, val args: List<Expression>) : ParseTree(loc) {
    override fun getChildren() = args
    override fun prettyString() = indent("Annotation", name, args)
}

class FunctionDefNode(
    loc: FileRangeLocation,
    val annotations: List<Annotation>,
    val modifiers: TopLevelModifiers,
    val name: String,
    val parameters: List<Parameter>,
    val outputType: ResourceLocation?,
    val code: Statement
): ParseTree(loc) {
    override fun getChildren() = annotations + parameters + code
    override fun prettyString() = indent("FunctionDef", annotations, modifiers, name, parameters, outputType, code)
}

class NamespaceDefNode(
    loc: FileRangeLocation,
    val name: ResourceLocation,
    val statements: List<ParseTree>
) : ParseTree(loc) {
    override fun getChildren() = statements
    override fun prettyString() = indent("NamespaceDef", name, statements)
}