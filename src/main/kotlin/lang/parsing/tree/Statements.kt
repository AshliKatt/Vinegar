package dev.ashli.vinegar.lang.parsing.tree

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.modifiers.TopLevelModifiers

sealed class Statement(location: FileRangeLocation) : ParseTree(location)

class BlockStatementNode(location: FileRangeLocation, val statements: List<Statement>): Statement(location) {
    override fun getChildren() = statements
    override fun prettyString() = indent("BlockStatement", statements)
}

class ExprStatementNode(location: FileRangeLocation, val expr: Expression) : Statement(location) {
    override fun getChildren() = listOf(expr)
    override fun prettyString() = indent("ExprStatement", expr)
}

class VarDeclarationNode(
    location: FileRangeLocation,
    val annotations: List<Annotation>,
    val modifiers: TopLevelModifiers,
    val varName: String,
    val type: ResourceLocation?,
    val initValue: Expression?
) : Statement(location) {
    override fun getChildren() = listOfNotNull(initValue)
    override fun prettyString() = indent("VarDeclaration", annotations, modifiers, varName, type, initValue)
}

class ConstDeclarationNode(
    location: FileRangeLocation,
    val annotations: List<Annotation>,
    val modifiers: TopLevelModifiers,
    val constName: String,
    val type: ResourceLocation?,
    val initValue: Expression
) : Statement(location) {
    override fun getChildren() = listOf(initValue)
    override fun prettyString() = indent("ConstDeclaration", annotations, modifiers, constName, type, initValue)
}

class IfStatementNode(
    location: FileRangeLocation,
    val condition: Expression,
    val mainBranch: Statement
) : Statement(location) {
    override fun getChildren() = listOf(condition, mainBranch)
    override fun prettyString() = indent("IfStatement", condition, mainBranch)
}

class IfElseStatementNode(
    location: FileRangeLocation,
    val condition: Expression,
    val mainBranch: Statement,
    val elseBranch: Statement
) : Statement(location) {
    override fun getChildren() = listOf(condition, mainBranch, elseBranch)
    override fun prettyString() = indent("IfElseStatement", condition, mainBranch, elseBranch)
}

class WhileStatementNode(
    location: FileRangeLocation,
    val condition: Expression,
    val mainBranch: Statement
) : Statement(location) {
    override fun getChildren() = listOf(condition, mainBranch)
    override fun prettyString() = indent("WhileStatement", condition, mainBranch)
}

class ForStatementNode(
    location: FileRangeLocation,
    val varName: String,
    val type: ResourceLocation?,
    val iterator: Expression,
    val mainBranch: Statement
) : Statement(location) {
    override fun getChildren() = listOf(iterator, mainBranch)
    override fun prettyString() = indent("ForStatement", varName, type, iterator, mainBranch)
}

class ReturnStatementNode(location: FileRangeLocation, val value: Expression?) : Statement(location) {
    override fun getChildren() = listOfNotNull(value)
    override fun prettyString() = indent("ReturnStatement", value)
}

class BreakStatementNode(location: FileRangeLocation) : Statement(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "BreakStatement"
}

class ContinueStatementNode(location: FileRangeLocation): Statement(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "ContinueStatement"
}