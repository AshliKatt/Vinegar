package dev.ashli.vinegar.lang.parsing.tree

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation

sealed class Expression(location: FileRangeLocation) : ParseTree(location)

class BoolNode(location: FileRangeLocation, val value: Boolean) : Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "Boolean($value)"
}

class StrNode(location: FileRangeLocation, val value: String) : Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "String($value)"
}

class TextNode(location: FileRangeLocation, val value: String) : Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "Text($value)"
}

class IntNode(location: FileRangeLocation, val value: Long) : Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "Int($value)"
}

class FloatNode(location: FileRangeLocation, val value: Long) : Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "Float($value)"
}

class ResourceLocationNode(location: FileRangeLocation, val resource: ResourceLocation): Expression(location) {
    override fun getChildren() = emptyList<ParseTree>()
    override fun prettyString() = "Resource($resource)"
}