package dev.ashli.vinegar.lang.parsing.tree

import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation

sealed class UnaryOperation(location: FileRangeLocation, val a: Expression) : Expression(location) {
    override fun getChildren() = listOf(a)
    override fun prettyString() = indent(this::class.simpleName!!, a)
}

sealed class BinaryOperation(location: FileRangeLocation, val a: Expression, val b: Expression) : Expression(location) {
    override fun getChildren() = listOf(a, b)
    override fun prettyString() = indent(this::class.simpleName!!, a, b)
}

class PlusNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class MinusNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class MultiplyNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class DivideNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class ModuloNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class NegateNode(loc: FileRangeLocation, a: Expression) : UnaryOperation(loc, a)
class BoolAndNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class BoolOrNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class BoolNotNode(loc: FileRangeLocation, a: Expression) : UnaryOperation(loc, a)
class GreaterEqNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class LessEqNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class GreaterNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class LessNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class EqualNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class NotEqualNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)
class IndexNode(loc: FileRangeLocation, a: Expression, b: Expression) : BinaryOperation(loc, a, b)

class FunctionCallNode(loc: FileRangeLocation, val function: ResourceLocation, val args: List<Expression>) : Expression(loc) {
    override fun getChildren() = args
    override fun prettyString() = indent("FunctionCall", function, args)
}

class PropertyAccessNode(loc: FileRangeLocation, val obj: Expression, val property: String) : Expression(loc) {
    override fun getChildren() = listOf(obj)
    override fun prettyString() = indent("PropertyAccess", obj, property)
}

class MethodCallNode(
    loc: FileRangeLocation,
    val obj: Expression,
    val name: String,
    val args: List<Expression>
) : Expression(loc) {
    override fun getChildren() = args + obj
    override fun prettyString() = indent("MethodCall", obj, name, args)
}

