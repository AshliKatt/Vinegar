package dev.ashli.vinegar.lang.parsing.tree

import dev.ashli.vinegar.lang.location.FileRangeLocation

/**
 * Represents a node in the parse tree
 */
sealed class ParseTree(val location: FileRangeLocation) {
    internal companion object {
        fun indent(name: String, vararg inner: Any?): String {
            return if (inner.isEmpty()) {
                "$name()"
            } else {
                "$name${indent("(", ")", *inner)}"
            }
        }

        private fun indent(prefix: String, postfix: String, vararg inner: Any?) : String {
            if (inner.isEmpty()) {
                return "$prefix$postfix"
            }

            return "$prefix\n${
                inner.joinToString(",\n") { tree ->
                    if (tree is List<*>) {
                        indent("[", "]", *tree.toTypedArray()).split("\n").joinToString("\n") {
                            "  $it"
                        }
                    } else {
                        tree.toString().split("\n").joinToString("\n") {
                            "  $it"
                        }
                    }
                }
            }\n$postfix"
        }
    }

    abstract fun getChildren(): List<ParseTree>

    abstract fun prettyString(): String

    override fun toString() = prettyString()
}