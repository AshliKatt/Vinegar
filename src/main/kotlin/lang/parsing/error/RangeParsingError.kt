package dev.ashli.vinegar.lang.parsing.error

import dev.ashli.vinegar.lang.error.RangeVinegarError
import dev.ashli.vinegar.lang.location.FileRangeLocation

open class RangeParsingError(
    message: String,
    loc: FileRangeLocation,
    help: String?
) : RangeVinegarError(message, loc, help) {
    class UnexpectedTokenError(
        message: String,
        loc: FileRangeLocation
    ) : RangeParsingError("Expected $message.", loc, null)

    class NoTopLevelError(
        err: String,
        loc: FileRangeLocation
    ) : RangeParsingError("$err are not allowed at top-level.", loc, "Consider moving it into a namespace.")

    class MisplacedImportError(
        loc: FileRangeLocation
    ) : RangeParsingError("Import statements must be at the top of the file.", loc,
        "Consider moving the import to line 1.")

    class IncompatibleVisibilityModifiersError(
        loc: FileRangeLocation
    ) : RangeParsingError("Only one visibility modifier is allowed on a single element.", loc,
        "Consider removing the extra visibility modifier.")

    class DuplicateModifiersError(
        loc: FileRangeLocation
    ) : RangeParsingError("Duplicate element modifier are not allowed.", loc,
        "Consider removing the duplicate modifier.")

    class NamespaceModifierError(
        loc: FileRangeLocation
    ) : RangeParsingError("Namespaces cannot have modifiers.", loc,
        "Remove the modifier(s).")

    class NamespaceAnnotationError(
        loc: FileRangeLocation
    ) : RangeParsingError("Namespaces cannot be annotated.", loc,
        "Remove the annotation(s).")
}