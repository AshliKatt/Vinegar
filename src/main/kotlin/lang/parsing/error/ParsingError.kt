package dev.ashli.vinegar.lang.parsing.error

import dev.ashli.vinegar.lang.error.VinegarError
import dev.ashli.vinegar.lang.location.FileLocation

open class ParsingError(
    message: String,
    loc: FileLocation,
    help: String?
) : VinegarError(message, loc, help) {
    class ExpectedTokenError(
        expected: String,
        loc: FileLocation
    ) : ParsingError("Unexpected end of file. Expected $expected.", loc, null)

    class RequiredConstInitializationError(
        loc: FileLocation
    ) : ParsingError("Constants must be initialized.", loc,
        "Place an equals sign with a value at the end of the declaration.")

    class RequiredVarInitializationError(
        loc: FileLocation
    ) : ParsingError("Variables must have an explicit type or initialized with a value.", loc,
        "Specify the variable's type explicitly or initialize it with a value.")

    class RequiredParamTypeError(
        loc: FileLocation
    ) : ParsingError("Function parameters must have an explicit type.", loc,
        "Specify the parameter's type explicitly with a colon.")
}