package dev.ashli.vinegar.lang.tokenization

import dev.ashli.vinegar.lang.location.FileLocation
import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.MutableFileLocation
import dev.ashli.vinegar.lang.tokenization.encoding.Decimal
import dev.ashli.vinegar.lang.tokenization.encoding.NumberBase
import dev.ashli.vinegar.lang.tokenization.error.TokenizationError
import dev.ashli.vinegar.lang.tokenization.error.TokenizationError.*
import dev.ashli.vinegar.lang.tokenization.settings.TokenizerSettings
import dev.ashli.vinegar.lang.tokenization.token.Token
import dev.ashli.vinegar.lang.tokenization.token.TokenType.*
import dev.ashli.vinegar.lang.tokenization.token.TokenType.Float
import dev.ashli.vinegar.lang.tokenization.token.TokenType.Int
import java.io.File
import kotlin.ArithmeticException
import kotlin.Boolean
import kotlin.Char
import kotlin.CharArray

/**
 * Used to convert code files into lists of Tokens.
 */
class Tokenizer private constructor(file: File, val settings: TokenizerSettings) {
    companion object {
        /**
         * Converts a code string into a list of tokens
         * @Throws TokenizationError If the code string is invalid
         */
        fun tokenize(file: File, settings: TokenizerSettings) = Tokenizer(file, settings).tokenize()
    }

    private val reader = file.bufferedReader()
    private val charBuffer = CharArray(256)
    private var bufferSize: kotlin.Int = 0
    private var index = 0
    private val out = mutableListOf<Token>()

    private val loc = MutableFileLocation(1, 0, file)


    /**
     * Tokenizes the source code, consuming this iterator. Updates and returns "out"
     * @throws TokenizationError If the input source code is invalid
     */
    private fun tokenize(): List<Token> {
        while (hasNext()) {
            val char = next()

            // Strictly int, checks for  0x, 0b, etc
            if (char == '0' && hasNext()) {
                val base = settings.charToBase(peek())

                if (base != null) {
                    next()
                    parseInt(base)
                    continue
                }
            }

            if (char.isDigit() || (char == '.' && hasNext() && (peek().isDigit() || peek() == '_'))) {
                parseNumber(char)
                continue
            }

            // Ignored chars
            if (settings.isIgnoredChar(char)) continue

            // Two char tokens
            if (hasNext()) {
                if (char == '/') {
                    if (peek() == '/') {
                        while (hasNext()) {
                            if (next() == '\n') break
                        }
                        out.add(Token(Newline, loc.immutable().range()))
                        continue

                    } else if (peek() == '*') {
                        while (hasNext()) {
                            if (next() == '*' && peek() == '/') break
                        }
                        next()
                        continue
                    }

                }

                val doubleChar = settings.doubleCharToken(char, peek())
                if (doubleChar != null) {
                    out.add(Token(doubleChar, loc.range(2).immutable()))
                    next()
                    continue
                }
            }

            // Single char tokens
            val singleChar = settings.singleCharToken(char)
            if (singleChar != null) {
                out.add(Token(singleChar, loc.range().immutable()))
                continue
            }

            // Strings
            if (settings.isStringDelimiter(char)) {
                parseString(char, false)
                continue
            }

            // Strings
            if ((char == 't' || char == 'T') && settings.isStringDelimiter(peek())) {
                parseString(next(), true)
                continue
            }

            // Idents
            if (settings.isIdentChar(char, true)) {
                val ident = StringBuilder(char.toString())
                val startLoc = loc.immutable()

                while (hasNext() && settings.isIdentChar(peek(), false)) {
                    ident.append(next())
                }

                out.add(Token(settings.keywordToken(ident.toString()), FileRangeLocation.between(startLoc, loc.immutable().add(0, 1))))
                continue
            }

            // Numbers
            throw UnexpectedCharError(char, loc.immutable())
        }

        reader.close()
        return out
    }

    /**
     * Strictly parses a non-base10 integer, expects iterator to have "0x" and whatnot already consumed
     */
    private fun parseInt(base: NumberBase) {
        val startLoc = loc.immutable().add(0, -1)

        val chars = collect(settings::isBaseNumerical)

        addInt(chars, base, startLoc)
    }

    /**
     * Parses a base10 number. Expects the first digit to have already been consumed and passed into this function
     */
    private fun parseNumber(initial: Char) {
        val startLoc = loc.immutable()

        val chars = collect(settings::isNumerical)
        chars.add(0, initial)

        val periodCount = chars.count { it == '.' }

        if (periodCount == 1) {
            addFloat(chars, startLoc)
        } else if (periodCount == 0) {
            if (chars.last() == 'f' || chars.last() == 'F') {
                chars.removeLast()
                addFloat(chars, startLoc)
            } else {
                addInt(chars, Decimal, startLoc)
            }
        } else {
            throw MalformedNumberError("Too many decimal points.", FileRangeLocation.between(startLoc, loc.immutable()))
        }
    }

    private fun addInt(chars: List<Char>, base: NumberBase, startLoc: FileLocation) {
        try {
            var hasDigit = false

            val value = Math.multiplyExact(chars.fold(0L) { acc, char ->
                if (char == '_') {
                    acc
                } else {
                    val digitValue = base.value(char)
                        ?: throw WrongBaseError(char, base, FileRangeLocation.between(startLoc, loc.immutable()).widen())

                    hasDigit = true
                    Math.addExact(Math.multiplyExact(acc, base.base()), digitValue)
                }
            }, 1000L)

            if (!hasDigit) throw MissingNumberError(FileRangeLocation.between(startLoc, loc.immutable()).widen())

            out.add(Token(Int(value), FileRangeLocation.between(startLoc, loc.immutable()).widen()))
        } catch (e: ArithmeticException) {
            throw IntOutOfRangeError(FileRangeLocation.between(startLoc, loc.immutable()).widen())
        }
    }

    private fun addFloat(chars: List<Char>, startLoc: FileLocation) {
        try {
            var value = 0L
            val iter = chars.iterator()
            var hasDecimal = false
            var hasDigit = false

            for (char in iter) {
                if (char == '.') {
                    hasDecimal = true
                    break
                } else if (char.isDigit()) {
                    value = Math.addExact(Math.multiplyExact(value, 10L), char.digitToInt().toLong())
                    hasDigit = true
                } else if (char != '_') {
                    throw InvalidDigitError(char, FileRangeLocation.between(startLoc, loc.immutable()).widen())
                }
            }

            value = Math.multiplyExact(value, 1000)

            if (hasDecimal) {
                var place = 100
                var hasDecimalDigit = false

                for (char in iter) {
                    if (char == '.') {
                        throw MalformedNumberError("Too many decimal points.", FileRangeLocation.between(startLoc, loc.immutable()))
                    } else if (char.isDigit()) {
                        value = Math.addExact(value, place * char.digitToInt().toLong())
                        if (place == 0) throw DecimalOutOfRangeError(FileRangeLocation.between(startLoc, loc.immutable()).widen())
                        place /= 10
                        hasDecimalDigit = true
                    } else if (char != '_') {
                        throw InvalidDigitError(char, FileRangeLocation.between(startLoc, loc.immutable()).widen())
                    }
                }

                if (!hasDecimalDigit) throw MissingDecimalError(FileRangeLocation.between(startLoc, loc.immutable()).widen())

                out.add(Token(Float(value), FileRangeLocation.between(startLoc, loc.immutable()).widen()))
            } else {
                if (!hasDigit) throw MissingNumberError(FileRangeLocation.between(startLoc, loc.immutable()).widen())

                out.add(Token(Float(value), FileRangeLocation.between(startLoc, loc.immutable()).widen()))
            }

        } catch (e: ArithmeticException) {
            throw IntOutOfRangeError(FileRangeLocation.between(startLoc, loc.immutable()).widen())
        }
    }

    /**
     * Returns the next char and progresses the iterator.
     * Modifies "col" and "line" vars to reflect the new iterator position
     */
    private fun next(): Char {
        val out = peek()
        index++

        if (out == '\n') {
            loc.col = 0
            loc.line++
        } else {
            loc.col++
        }

        return out
    }

    /**
     * Returns the next char without progressing the iterator
     */
    private fun peek(): Char {
        if (index >= bufferSize) {
            bufferSize = reader.read(charBuffer)
            index = 0
        }

        return charBuffer[index]
    }

    /**
     * Returns true or false depending on if there are more chars to iterate over in the source code
     */
    private fun hasNext(): Boolean {
        if (bufferSize == -1) {
            return false
        } else if (index < bufferSize) {
            return true
        } else {
            bufferSize = reader.read(charBuffer)
            index = 0
            return bufferSize != -1
        }
    }

    /**
     * Greedily collects as many characters as possible into a list based on a discriminator function
     */
    private fun collect(fn: (Char) -> Boolean): MutableList<Char> {
        val list = mutableListOf<Char>()
        while (hasNext() && fn(peek())) {
            list.add(next())
        }
        return list
    }

    /**
     * Parses a string, expects the first string delimiter to have already been consumed and passed into this function
     */
    private fun parseString(delimiter: Char, isText: Boolean) {
        val str = StringBuilder()
        val startLoc = loc.immutable()

        var escaped = false

        while (hasNext()) {
            val strChar = peek()

            if (escaped) {
                next()
                str.append(settings.stringEscapes(strChar, delimiter)
                    ?: throw InvalidEscapeCode(strChar, FileRangeLocation.between(startLoc.add(0, -1), loc.immutable().add(0, 1)))
                )
                escaped = false
            } else {
                if (strChar == '\n') throw UnclosedStringError(FileRangeLocation.between(startLoc, loc.immutable()).widen())
                next()
                when (strChar) {
                    '\\' -> escaped = true
                    delimiter -> break
                    else -> str.append(strChar)
                }
            }
        }

        val type = if (isText) Text(str.toString()) else Str(str.toString())
        out.add(Token(type, FileRangeLocation.between(startLoc, loc.immutable()).widen()))
    }
}
