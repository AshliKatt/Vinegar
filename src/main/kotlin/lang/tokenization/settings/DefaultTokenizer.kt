package dev.ashli.vinegar.lang.tokenization.settings

import dev.ashli.vinegar.lang.tokenization.encoding.Binary
import dev.ashli.vinegar.lang.tokenization.encoding.Hexadecimal
import dev.ashli.vinegar.lang.tokenization.encoding.NumberBase
import dev.ashli.vinegar.lang.tokenization.encoding.Octal
import dev.ashli.vinegar.lang.tokenization.token.TokenType
import dev.ashli.vinegar.lang.tokenization.token.TokenType.*
import dev.ashli.vinegar.lang.tokenization.token.TokenType.Function

object DefaultTokenizer : TokenizerSettings {
    override fun singleCharToken(c: Char): TokenType? {
        return when (c) {
            '!' -> Exclaim
            '%' -> Percent
            '*' -> Asterisk
            '(' -> OpenParen
            ')' -> CloseParen
            '-' -> Minus
            '+' -> Plus
            '=' -> Assign
            '<' -> LessThan
            '>' -> GreaterThan
            ',' -> Comma
            '.' -> Period
            '/' -> Slash
            ':' -> Colon
            ';' -> Semicolon
            '[' -> OpenBracket
            ']' -> CloseBracket
            '{' -> OpenBrace
            '}' -> CloseBrace
            '@' -> Annotate
            '\n' -> Newline
            else -> null
        }
    }

    override fun doubleCharToken(c1: Char, c2: Char): TokenType? {
        return when ("$c1$c2") {
            "==" -> Equals
            ">=" -> GreaterEquals
            "<=" -> LessEquals
            "!=" -> NotEquals
            "&&" -> BoolAnd
            "||" -> BoolOr
            "->" -> Arrow
            "::" -> DoubleColon
            else -> null
        }
    }

    override fun keywordToken(s: String): TokenType {
        return when (s) {
            "fn" -> Function
            "let" -> Let
            "const" -> Const
            "while" -> While
            "for" -> For
            "if" -> If
            "else" -> Else
            "return" -> Return
            "break" -> Break
            "continue" -> Continue
            "in" -> In
            "true" -> True
            "false" -> False
            "namespace" -> Namespace
            "public" -> Public
            "private" -> Private
            "internal" -> Internal
            "test" -> Test
            "import" -> Import
            else -> Identifier(s)
        }
    }

    override fun isIgnoredChar(c: Char): Boolean {
        return c.isWhitespace() && c != '\n'
    }

    override fun isStringDelimiter(c: Char): Boolean {
        return c == '\'' || c == '"'
    }

    override fun isIdentChar(c: Char, first: Boolean): Boolean {
        return c.isLetter() || c == '_' || (c.isDigit() && !first)
    }

    override fun stringEscapes(c: Char, delimiter: Char): Char? {
        return when (c) {
            'b' -> '\b'
            'n' -> '\n'
            'r' -> '\r'
            't' -> '\t'
            '\n' -> '\n'
            '\\' -> '\\'
            delimiter -> delimiter
            else -> null
        }
    }

    override fun isNumerical(c: Char): Boolean {
        return c.isLetterOrDigit() || c == '_' || c == '.'
    }

    override fun isBaseNumerical(c: Char): Boolean {
        return c.isLetterOrDigit() || c == '_'
    }

    override fun charToBase(c: Char): NumberBase? {
        return when (c) {
            'b' -> Binary
            'o' -> Octal
            'x' -> Hexadecimal
            else -> null
        }
    }
}