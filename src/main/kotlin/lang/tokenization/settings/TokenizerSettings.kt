package dev.ashli.vinegar.lang.tokenization.settings

import dev.ashli.vinegar.lang.tokenization.encoding.NumberBase
import dev.ashli.vinegar.lang.tokenization.token.TokenType

interface TokenizerSettings {
    fun singleCharToken(c: Char): TokenType?
    fun doubleCharToken(c1: Char, c2: Char): TokenType?
    fun keywordToken(s: String): TokenType
    fun isIgnoredChar(c: Char): Boolean
    fun isStringDelimiter(c: Char): Boolean
    fun isIdentChar(c: Char, first: Boolean): Boolean
    fun stringEscapes(c: Char, delimiter: Char): Char?
    fun isNumerical(c: Char): Boolean
    fun isBaseNumerical(c: Char): Boolean
    fun charToBase(c: Char): NumberBase?
}