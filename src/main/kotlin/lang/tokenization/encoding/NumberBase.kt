package dev.ashli.vinegar.lang.tokenization.encoding

import java.util.*

interface NumberBase {
    /**
     * Returns if a character is a digit in the string representation of this base
     */
    fun isDigit(c: Char): Boolean = value(c) != null

    /**
     * Returns the base of this object, 10 for base10, etc.
     */
    fun base(): Long

    /**
     * Returns the value of a specific digit, null if invalid.
     */
    fun value(c: Char): Long?

    fun toStringLowercase(): String {
        return this.toString().lowercase(Locale.getDefault())
    }
}