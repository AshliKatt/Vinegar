package dev.ashli.vinegar.lang.tokenization.encoding

object Decimal : NumberBase {
    override fun base() = 10L

    override fun value(c: Char): Long? {
        return when (c) {
            in '0'..'9' -> c.digitToInt().toLong()
            else -> null
        }
    }

    override fun toString() = "Decimal"
}