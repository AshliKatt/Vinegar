package dev.ashli.vinegar.lang.tokenization.encoding

object Octal : NumberBase {
    override fun base() = 8L

    override fun value(c: Char): Long? {
        return when (c) {
            in '0'..'7' -> c.digitToInt().toLong()
            else -> null
        }
    }

    override fun toString() = "Octal"
}