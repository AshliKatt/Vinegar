package dev.ashli.vinegar.lang.tokenization.encoding

object Binary : NumberBase {
    override fun base() = 2L

    override fun value(c: Char): Long? {
        return when (c) {
            in '0'..'1' -> c.digitToInt().toLong()
            else -> null
        }
    }

    override fun toString() = "Binary"
}