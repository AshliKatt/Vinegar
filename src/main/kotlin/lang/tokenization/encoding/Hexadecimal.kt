package dev.ashli.vinegar.lang.tokenization.encoding

object Hexadecimal : NumberBase {
    override fun base() = 16L

    override fun value(c: Char): Long? {
        return when (c) {
            in '0'..'9' -> c.digitToInt().toLong()
            in 'A'..'F' -> c.digitToInt(16).toLong()
            in 'a'..'f' -> c.digitToInt(16).toLong()
            else -> null
        }
    }

    override fun toString() = "Hexadecimal"
}