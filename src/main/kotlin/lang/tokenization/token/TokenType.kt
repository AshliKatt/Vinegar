package dev.ashli.vinegar.lang.tokenization.token

sealed class TokenType(private val name: String) {
    override fun toString() = name

    fun matches(other: TokenType) = this::class == other::class


    // Meta
    class Int(val value: Long) : TokenType(value.toString())
    class Float(val value: Long) : TokenType(value.toString())
    class Str(val value: String) : TokenType("\"${value}\"")
    class Text(val value: String) : TokenType("T\"${value}\"")
    class Identifier(val value: String) : TokenType(value)


    // Keyword tokens
    object Function : TokenType("fn")
    object Let : TokenType("let")
    object Const : TokenType("const")
    object While : TokenType("while")
    object For : TokenType("for")
    object If : TokenType("if")
    object Else : TokenType("else")
    object Return : TokenType("return")
    object Break : TokenType("break")
    object Continue : TokenType("continue")
    object In : TokenType("in")
    object True : TokenType("true")
    object False : TokenType("false")
    object Namespace : TokenType("namespace")
    object Import : TokenType("import")
    object Test : TokenType("test")
    object Public : TokenType("public")
    object Private : TokenType("private")
    object Internal : TokenType("internal")


    // Two char tokens
    object Equals : TokenType("==")
    object GreaterEquals : TokenType(">=")
    object LessEquals : TokenType("<=")
    object NotEquals : TokenType("!=")
    object Arrow : TokenType("->")
    object BoolAnd : TokenType("&&")
    object BoolOr : TokenType("||")



    // Single char tokens
    object Exclaim : TokenType("!")
    object Percent : TokenType("%")
    object Asterisk : TokenType("*")
    object OpenParen : TokenType("(")
    object CloseParen : TokenType(")")
    object Minus : TokenType("-")
    object Plus : TokenType("+")
    object Assign : TokenType("=")
    object LessThan : TokenType("<")
    object GreaterThan : TokenType(">")
    object Comma : TokenType(",")
    object Period : TokenType(".")
    object Slash : TokenType("/")
    object Colon : TokenType(":")
    object DoubleColon : TokenType("::")
    object Semicolon : TokenType(";")
    object OpenBracket : TokenType("[")
    object CloseBracket : TokenType("]")
    object OpenBrace : TokenType("{")
    object CloseBrace : TokenType("}")
    object Newline : TokenType("newline")
    object Annotate : TokenType("@")
}