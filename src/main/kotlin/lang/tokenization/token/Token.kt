package dev.ashli.vinegar.lang.tokenization.token

import dev.ashli.vinegar.lang.location.FileRangeLocation

data class Token(
    val type: TokenType,
    val loc: FileRangeLocation
) {
    override fun toString() = "$type"

    fun matches(vararg other: TokenType) = other.any(this.type::matches)
    fun matches(other: Token) = this.type.matches(other.type)
}