package dev.ashli.vinegar.lang.tokenization.error

import dev.ashli.vinegar.lang.error.RangeVinegarError
import dev.ashli.vinegar.lang.location.FileLocation
import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.number.formatNum

import dev.ashli.vinegar.lang.tokenization.encoding.NumberBase

sealed class TokenizationError(
    message: String,
    loc: FileRangeLocation,
    help: String?
) : RangeVinegarError(message, loc, help) {
    class InvalidEscapeCode(
        char: Char,
        loc: FileRangeLocation
    ) : TokenizationError("Illegal escape in this context: '\\$char'.", loc,
        "Allowed escapes are: '\\b', '\\n', '\\r', '\\t', '\\\\', and either '\\\"' or '\\''. (Depending on string delimiter)")


    class UnclosedStringError(
        loc: FileRangeLocation
    ) : TokenizationError("Unclosed string literal.", loc,
        "You may add newlines to a string with '\\n', or by placing a backslash immediately before the line break.")

    class MissingNumberError(
        loc: FileRangeLocation
    ) : TokenizationError("Number must include at least one digit.", loc, null)

    class MissingDecimalError(
        loc: FileRangeLocation
    ) : TokenizationError("Number must include at least one digit after the decimal point.", loc, null)

    class IntOutOfRangeError(
        loc: FileRangeLocation
    ) : TokenizationError("Number out of allowed range.", loc,
        "Numbers must be between ${formatNum(Long.MIN_VALUE + 1L)} and ${formatNum(Long.MAX_VALUE)}.")

    class DecimalOutOfRangeError(
        loc: FileRangeLocation
    ) : TokenizationError("Numbers are only allowed to have 3 decimal points.", loc,
        "Truncate the number to 3 decimal places.")

    class WrongBaseError(
        char: Char,
        base: NumberBase,
        loc: FileRangeLocation
    ) : TokenizationError(
        "Character '$char' is not a digit in ${base.toStringLowercase()}.", loc,
        "Remove or correct the invalid digit, or separate the character from the ${base.toStringLowercase()} number.")

    // Like WrongBaseError but for decimal
    class InvalidDigitError(
        char: Char,
        loc: FileRangeLocation
    ) : TokenizationError(
        "Character '$char' is not a valid digit.", loc,
        if (char == 'f' || char == 'F') "'$char' is not needed here because the number already includes a decimal point."
        else "Remove or correct the invalid digit, or separate the character from the number."
    )

    class MalformedNumberError(
        msg: String,
        loc: FileRangeLocation
    ) : TokenizationError(
        "Number is malformed: $msg", loc,null)

    class UnexpectedCharError(
        char: Char,
        loc: FileLocation
    ) : TokenizationError("Unexpected character '$char' in source code.", loc.range(), null)
}