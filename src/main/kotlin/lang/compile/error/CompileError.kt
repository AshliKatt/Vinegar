package dev.ashli.vinegar.lang.compile.error

import dev.ashli.vinegar.lang.error.VinegarError
import dev.ashli.vinegar.lang.location.FileLocation

open class CompileError(
    message: String,
    loc: FileLocation,
    help: String?
) : VinegarError(message, loc, help) {

}