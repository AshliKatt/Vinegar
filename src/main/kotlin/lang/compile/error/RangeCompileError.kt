package dev.ashli.vinegar.lang.compile.error

import dev.ashli.vinegar.lang.error.RangeVinegarError
import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation

open class RangeCompileError(
    message: String,
    loc: FileRangeLocation,
    help: String?
) : RangeVinegarError(message, loc, help) {
    class DuplicateResourceError(
        location: ResourceLocation,
        loc: FileRangeLocation
    ) : RangeCompileError("Resource $location already exists.", loc, "Rename the duplicate resource.")

    class DuplicateLocalVarError(
        variable: String,
        loc: FileRangeLocation
    ) : RangeCompileError("Duplicate local variable $variable.", loc, "Rename the duplicate resource.")
}