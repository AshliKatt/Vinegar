package dev.ashli.vinegar.lang.compile.tracking

import dev.ashli.vinegar.lang.compile.error.RangeCompileError
import dev.ashli.vinegar.lang.location.FileRangeLocation
import dev.ashli.vinegar.lang.location.ResourceLocation

/**
 * Holds valid local variables and their types
 */
class LocalResourceContext(private val parent: LocalResourceContext? = null) {
    private val resources = mutableMapOf<String, ResourceLocation>()

    operator fun get(resource: String): ResourceLocation? {
        return resources[resource] ?: parent?.get(resource)
    }

    fun has(resource: String): Boolean {
        return resources.containsKey(resource) || parent?.has(resource) ?: false
    }

    fun declare(location: FileRangeLocation, resource: String, parseTree: ResourceLocation) {
        if (has(resource)) {
            throw RangeCompileError.DuplicateLocalVarError(resource, location)
        }

        // Shadowing allowed

        resources[resource] = parseTree
    }
}