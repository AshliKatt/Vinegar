package dev.ashli.vinegar.lang.compile.tracking

import dev.ashli.vinegar.lang.compile.error.RangeCompileError
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.tree.FunctionDefNode

/**
 * Holds all global (non-local) resource contexts in a program
 */
class GlobalResourceContext {
    private val allResources = mutableMapOf<ResourceLocation, UnlinkedParseTree>()

    operator fun get(resource: ResourceLocation): UnlinkedParseTree? {
        return allResources[resource]
    }

    fun has(resource: ResourceLocation): Boolean {
        return allResources.containsKey(resource)
    }

    operator fun set(resource: ResourceLocation, parseTree: UnlinkedParseTree) {
        if (has(resource)) {
            throw RangeCompileError.DuplicateResourceError(resource, parseTree.tree.location)
        }

        allResources[resource] = parseTree
    }

    /**
     * Gets all "entrance" functions - ie: event functions
     */
    internal fun getEntranceResources(): Map<ResourceLocation, List<UnlinkedParseTree>> {
        val map = mutableMapOf<ResourceLocation, List<UnlinkedParseTree>>()

        allResources.values.forEach { node ->
            if (node.tree is FunctionDefNode) { // If it's a function
                //if (node.tree.annotations.any { it.name }) { // If it has EventHandler annotation

                //}
            }
        }

        return map
    }

    /**
     * Takes an incomplete ResourceLocation and resolves its fully qualified name using its location in
     * the codebase along with the imports that apply to it.
     * @param name The incomplete ResourceLocation that will be resolved
     * @param location The location of the name from which to search from
     * @param extras Any additional imports to search
     * @return The full location of a resource
     * @throws RangeCompileError If no valid resource can be found using the input location
     */
    //fun resolve(name: ResourceLocation, location: ResourceLocation, extras: List<ResourceLocation>) : ResourceLocation {
        
    //}
}