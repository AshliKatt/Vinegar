package dev.ashli.vinegar.lang.compile.tracking

import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.tree.ParseTree

/**
 * Represents a parse tree that has not yet been linked.
 */
class UnlinkedParseTree(val tree: ParseTree, val imports: List<ResourceLocation>)