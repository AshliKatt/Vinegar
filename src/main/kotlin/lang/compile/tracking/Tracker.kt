package dev.ashli.vinegar.lang.compile.tracking

import dev.ashli.vinegar.lang.compile.error.RangeCompileError.DuplicateResourceError
import dev.ashli.vinegar.lang.error.warning.RangeVinegarWarning.*
import dev.ashli.vinegar.lang.location.MutableResourceLocation
import dev.ashli.vinegar.lang.location.ResourceLocation
import dev.ashli.vinegar.lang.parsing.output.ParserOutput
import dev.ashli.vinegar.lang.parsing.tree.*
import dev.ashli.vinegar.parsing.tree.*

/**
 * Tracks all parse trees' top-level structures for later compilation
 */
class Tracker private constructor(private val trees: List<ParserOutput>) {
    companion object {
        fun trackTrees(trees: List<ParserOutput>) = Tracker(trees).linkAll()
    }

    /**
     * The current resource location in the program.
     */
    private val currentLocation = MutableResourceLocation()

    /**
     * A list of unlinked resources.
     */
    private val context = GlobalResourceContext()

    /**
     * Registers a resource into the validResources map.
     */
    private fun registerResource(resource: ResourceLocation, tree: UnlinkedParseTree) {
        if (context.has(resource)) {
            throw DuplicateResourceError(resource, tree.tree.location)
        }

        context[resource] = tree
    }

    /**
     * Links all input files
     */
    private fun linkAll(): GlobalResourceContext {
        // Scan all trees and organize all resources into relevant locations
        trees.forEach { output ->
            output.trees.forEach {
                track(it, output.imports)
            }
        }

        return context
        //println(validResources)
    }

    /**
     * Scan a tree and add all of its resources (functions, global vars, etc) to relevant maps
     */
    private fun track(tree: ParseTree, imports: List<ResourceLocation>) {
        when (tree) {
            is NamespaceDefNode -> {
                currentLocation.append(tree.name)

                tree.getChildren().forEach {
                    track(it, imports)
                }

                repeat(tree.name.path.size) {
                    currentLocation.toParent()
                }
            }

            is VarDeclarationNode -> {
                InvalidVarFormat(tree).raise()
                registerResource(currentLocation.with(tree.varName), UnlinkedParseTree(tree, imports))
            }

            is ConstDeclarationNode -> {
                InvalidConstFormat(tree).raise()
                registerResource(currentLocation.with(tree.constName), UnlinkedParseTree(tree, imports))
            }

            is FunctionDefNode -> {
                InvalidFunctionFormat(tree).raise()
                registerResource(currentLocation.with(tree.name), UnlinkedParseTree(tree, imports))
            }

            else -> {}
        }
    }
}