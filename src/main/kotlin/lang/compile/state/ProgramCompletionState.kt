package dev.ashli.vinegar.lang.compile.state

import dev.ashli.vinegar.lang.error.warning.WarningManager
import dev.ashli.vinegar.printing.Format.Companion.LOGO
import dev.ashli.vinegar.printing.Format.Companion.LOGO_ERR
import dev.ashli.vinegar.printing.Format.Companion.LOGO_WARN
import dev.ashli.vinegar.printing.Format.Companion.RESET

sealed interface ProgramCompletionState {
    fun print()
    fun printLine() {
        this.print()
        println()
    }

    data object Success : ProgramCompletionState {
        override fun print() {
            print("${LOGO}Vinegar:${RESET} Program compiled successfully.")
        }
    }

    data object Warning : ProgramCompletionState {
        override fun print() {
            val warnings = WarningManager.warningCount()

            if (warnings == 1) {
                print("${LOGO_WARN}Vinegar:${RESET} Program compiled with 1 warning.")
            } else if (warnings > 1) {
                print("${LOGO_WARN}Vinegar:${RESET} Program compiled with $warnings warnings.")
            }

        }
    }

    data object Error : ProgramCompletionState {
        override fun print() {
            val warnings = WarningManager.warningCount()

            print("${LOGO_ERR}Vinegar:${RESET} Program failed to compile due to an error.")

            if (warnings == 1) {
                print(" 1 warning was raised.")
            } else if (warnings > 1) {
                print(" $warnings warnings were raised.")
            }
        }
    }
}